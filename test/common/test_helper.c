/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "test_helper.h"
#include "mod_pcm_svc.h"
#include "mod_pcm_rpc.h"

extern bool __real_pcm_dm_for_each(amxd_object_t* object, bool usersetting, bool changed, dm_for_each_cb cb, void* userdata);
extern bool __real_pcm_upc_for_each(const pcm_upc_t* data, upc_forEach_cb cb, void* userdata);

static bool register_called = false;
static bool unregister_called = false;
bool export_to_fail = false;
bool import_to_fail = false;

#define COPT_NAME "name"
#define TEMP_DIR "/tmp/mod_pcm_svc"

static test_helper_t helper;
static void test_helper_create_delfilepath(void);
static int rmrf(const amxc_string_t* path);

void test_helper_call_clear(void) {
    register_called = false;
    unregister_called = false;
}

bool test_helper_was_register_called(void) {
    return register_called;
}

bool test_helper_was_unregister_called(void) {
    return unregister_called;
}

void set_export_to_fail(bool request) {
    export_to_fail = request;
}

void set_import_to_fail(bool request) {
    import_to_fail = request;
}

void test_helper_setup(const char* odl, bool load_dm_events) {
    fprintf(stderr, "TEST-SETUP-ENTER: %s\n", __func__);

    amxd_dm_t* dm = NULL;
    amxo_parser_t* parser = NULL;
    amxd_object_t* root_obj = NULL;
    amxb_bus_ctx_t* bus_ctx = NULL;

    /* start with a clean TEMP_DIR (if present) */
    amxc_string_t tempDir;
    amxc_string_init(&tempDir, 0);
    amxc_string_set(&tempDir, TEMP_DIR);
    rmrf(&tempDir);
    amxc_string_clean(&tempDir);

    assert_int_equal(amxd_dm_new(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_new(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    amxp_sigmngr_enable(&dm->sigmngr, load_dm_events);
    assert_int_equal(amxo_parser_parse_file(parser, odl, root_obj), 0);
    amxp_sigmngr_enable(&dm->sigmngr, true);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    assert_int_equal(amxo_connection_add(parser,
                                         amxb_get_fd(bus_ctx),
                                         connection_read,
                                         "dummy:/tmp/dummy.sock",
                                         AMXO_BUS,
                                         bus_ctx)
                     , 0);
    assert_int_equal(amxb_register(bus_ctx, dm), 0);

    helper.dm = dm;
    helper.parser = parser;
    assert_int_equal(_main(AMXO_START, dm, parser), 0);

    // Ignore SIGALRM -> used for the timeout timer.
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);
    sigprocmask(SIG_BLOCK, &mask, NULL);

    test_helper_handle_events();
    app_start(NULL, NULL, NULL);

    assert_true(test_helper_was_register_called());

    /* handle events caused by initial populate */
    test_helper_handle_events();

    /* create the delfilepath */
    test_helper_create_delfilepath();

    fprintf(stderr, "TEST-SETUP-EXIT: %s\n", __func__);
}

void test_helper_teardown(void) {
    fprintf(stderr, "TEST-TEARDOWN-ENTER: %s\n", __func__);

    assert_int_equal(_main(AMXO_STOP, helper.dm, helper.parser), 0);

    assert_true(test_helper_was_unregister_called());
    test_helper_call_clear();

    amxo_resolver_import_close_all();
    assert_int_equal(test_unregister_dummy_be(), 0);
    amxd_dm_delete(&(helper.dm));
    amxo_parser_delete(&(helper.parser));
    amxc_string_clean(&(helper.delfilepath));

    fprintf(stderr, "TEST-TEARDOWN-EXIT: %s\n", __func__);
}

void test_helper_handle_events(void) {
    fprintf(stderr, "TEST-DEBUG: Handling events ");
    while(amxp_signal_read() == 0) {
        fprintf(stderr, ".");
    }
    fprintf(stderr, "\n");
}

void test_helper_read_sigalrm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        fprintf(stderr, "TEST-DEBUG: Got SIGALRM %s\n", __func__);
        amxp_timers_calculate();
        amxp_timers_check();
    } else {
        fprintf(stderr, "TEST-ERROR: Read unexpected signal %s\n", __func__);
    }
}

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     UNUSED int timeout) {
    int rc = 0;

    when_str_empty(object, exit);
    when_null(args, exit);
    when_null(bus_ctx, exit);

    fprintf(stderr, "TEST-DEBUG: method:[%s] - (%s)\n", method, __func__);

    fprintf(stderr, "TEST-DEBUG: args: - (%s)\n", __func__);
    amxc_var_dump(args, STDERR_FILENO);

    amxc_var_set_type(ret, AMXC_VAR_ID_NULL);

    const char* name = GETP_CHAR(args, "name");

    if(0 == strcmp(method, "registerSvc")) {
        assert_true((0 == strcmp(name, "dummyplugin_DummyService")) ||
                    (0 == strcmp(name, "dummycustomconfig_DummyService")) ||
                    (0 == strcmp(name, "dummyafterupdate_DummyService")));

        fprintf(stderr, "TEST-DEBUG: validating register data - (%s)\n", __func__);
        amxc_var_t* data = amxc_var_get_key(args, "data", AMXC_VAR_FLAG_DEFAULT);
        assert_non_null(data);
        amxc_var_t* info = amxc_var_get_key(data, "info", AMXC_VAR_FLAG_DEFAULT);
        assert_non_null(info);
        const char* dmRoot = GETP_CHAR(info, "dmRoot");
        assert_string_equal(dmRoot, "DummyService");
        assert_int_equal(GETP_INT32(info, "priority"), 0);
        if(0 == strcmp(name, "dummycustomconfig_DummyService")) {
            assert_string_equal(GETP_CHAR(info, "rpcImport"), "DummyImport");
            assert_string_equal(GETP_CHAR(info, "rpcExport"), "DummyExport");
        } else {
            assert_string_equal(GETP_CHAR(info, "rpcImport"), "PcmImport");
            assert_string_equal(GETP_CHAR(info, "rpcExport"), "PcmExport");
        }
        assert_int_equal(GETP_BOOL(info, "outOfOrder"), true);
        assert_string_equal(GETP_CHAR(info, "version"), "1.0");
        amxc_var_t* schemes = amxc_var_get_key(data, "schemes", AMXC_VAR_FLAG_DEFAULT);
        assert_non_null(schemes);
        amxc_var_t* firstscheme = amxc_var_get_first(schemes);
        assert_string_equal(GETP_CHAR(firstscheme, "id"), dmRoot);

        fprintf(stderr, "TEST-DEBUG: register called - (%s)\n", __func__);
        register_called = true;
        if(0 == strcmp(GETP_CHAR(args, "name"), "dummyafterupdate_DummyService")) {
            fprintf(stderr, "TEST-DEBUG: register with import for dummyafterupdate  - (%s)\n", __func__);

            amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
            amxc_var_t* htable = amxc_var_add(amxc_htable_t, ret, NULL);
            amxc_var_t* data = amxc_var_add_key(amxc_htable_t, htable, "data", NULL);
            amxc_var_add_key(cstring_t, data, "type", "upc");
            amxc_var_add_key(cstring_t, data, "version", "1.0");
            amxc_var_t* add = amxc_var_add_key(amxc_htable_t, data, "add", NULL);
            amxc_var_t* delete = amxc_var_add_key(amxc_htable_t, data, "delete", NULL);
            amxc_var_t* set = amxc_var_add_key(amxc_htable_t, data, "set", NULL);
            amxc_var_t* hgwconfig = amxc_var_add_key(amxc_htable_t, set, "hgwconfig", NULL);
            amxc_var_t* dummyservice = amxc_var_add_key(amxc_htable_t, hgwconfig, "DummyService", NULL);
            amxc_var_add_key(cstring_t, dummyservice, "Name", "DummyAfterUpdate");
        }
        rc = 0;
    } else if(0 == strcmp(method, "unregisterSvc")) {
        fprintf(stderr, "TEST-DEBUG: unregister called - (%s)\n", __func__);
        unregister_called = true;
        assert_true((0 == strcmp(GETP_CHAR(args, "name"), "dummycustomconfig_DummyService")) ||
                    (0 == strcmp(GETP_CHAR(args, "name"), "dummyplugin_DummyService")) ||
                    (0 == strcmp(GETP_CHAR(args, "name"), "dummyafterupdate_DummyService")));
        amxc_var_clean(args);
    } else if(0 == strcmp(method, "PcmImport")) {
        fprintf(stderr, "TEST-DEBUG: validating restoring data - (%s)\n", __func__);
        amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), object);
        pcm_rpc_import(dummy_object, NULL, args, NULL);
    } else {
        fprintf(stderr, "TEST-ERROR: Unexpected call [%s] - (%s)\n", method, __func__);
        rc = 1;
    }
exit:
    return rc;
}

bool __wrap_pcm_dm_for_each(amxd_object_t* object, bool usersetting, bool changed, dm_for_each_cb cb, void* userdata) {
    if(export_to_fail) {
        export_to_fail = false;
        return false;
    } else {
        return __real_pcm_dm_for_each(object, usersetting, changed, cb, userdata);
    }
}

bool __wrap_pcm_upc_for_each(const pcm_upc_t* data, upc_forEach_cb cb, void* userdata) {
    if(import_to_fail) {
        import_to_fail = false;
        return false;
    } else {
        return __real_pcm_upc_for_each(data, cb, userdata);
    }

}

static void test_helper_create_delfilepath(void) {
    amxc_string_init(&helper.delfilepath, 0);

    /* get the pcm delete file path */
    char* directory = pcm_svc_get_directory();
    assert_non_null(directory);
    assert_string_not_equal(directory, "");

    amxo_parser_t* parser = helper.parser;
    const char* name = GETP_CHAR(&parser->config, COPT_NAME);
    assert_non_null(name);
    assert_string_not_equal(name, "");

    amxc_string_setf(&helper.delfilepath, "%s/%s-del.json", directory, name);
    free(directory);
}

amxd_dm_t* test_helper_get_dm(void) {
    return helper.dm;
}

amxo_parser_t* test_helper_get_parser(void) {
    return helper.parser;
}

char* test_helper_get_delfilepath(void) {
    return helper.delfilepath.buffer;
}

static int rmrf(const amxc_string_t* path) {
    assert_non_null(path);

    struct stat st;
    int ret = stat(path->buffer, &st);
    if(ret == -1) {
        return -1;
    }

    amxc_string_t tempFile;
    amxc_string_init(&tempFile, 0);
    /* if it is a directory, also remove all the file in that dir*/
    if(S_ISDIR(st.st_mode) > 0) {
        DIR* d = opendir(path->buffer);
        assert_non_null(d);

        struct dirent* dir = NULL;
        while((dir = readdir(d)) != NULL) {
            if(dir->d_name[0] == '.') {
                continue;
            }
            amxc_string_appendf(&tempFile, "%s/%s", path->buffer, dir->d_name);
            rmrf(&tempFile);
            amxc_string_clean(&tempFile);
        }
        closedir(d);
    }
    ret = remove(path->buffer);
    amxc_string_clean(&tempFile);

    return ret;
}
