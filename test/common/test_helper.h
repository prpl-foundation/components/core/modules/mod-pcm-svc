/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc_variant.h>
#include <amxc/amxc_lqueue.h>
#include <amxc/amxc_string.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_dm.h>
#include <amxc/amxc_rbuffer.h>
#include <amxc/amxc_astack.h>
#include <amxc/amxc_lstack.h>
#include <amxo/amxo.h>
#include <amxp/amxp_slot.h>
#include <amxb/amxb.h>
#include <amxb/amxb_be.h>
#include <amxb/amxb_register.h>

#include <setjmp.h>
#include <cmocka.h>

#include "dummy_backend.h"
#include "mod_pcm_dm.h"
#include "mod_pcm_upc.h"

typedef struct {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxc_string_t delfilepath;
} test_helper_t;

void test_helper_setup(const char* odl, bool load_dm_events);
void test_helper_teardown(void);
void test_helper_handle_events(void);
void test_helper_read_sigalrm(void);
void set_export_to_fail(bool request);
void set_import_to_fail(bool request);

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout);

bool __wrap_pcm_dm_for_each(amxd_object_t* object, bool usersetting, bool changed, dm_for_each_cb cb, void* userdata);
bool __wrap_pcm_upc_for_each(const pcm_upc_t* data, upc_forEach_cb cb, void* userdata);

amxd_dm_t* test_helper_get_dm(void);
amxo_parser_t* test_helper_get_parser(void);
char* test_helper_get_delfilepath(void);
bool test_helper_was_unregister_called(void);
void test_helper_call_clear(void);
bool test_helper_was_register_called(void);
