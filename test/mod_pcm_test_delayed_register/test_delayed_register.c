/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include "../common/test_helper.h"
#include "test_delayed_register.h"
#include "mod_pcm_inst.h"
#include "mod_pcm_svc.h"

#include <stdlib.h>
#include <stdio.h>
static amxd_dm_t dm;
static amxo_parser_t parser;
static amxd_object_t* root_obj = NULL;
static amxb_bus_ctx_t* bus_ctx = NULL;

static const char* odl_mock_pcm_manager = "mock_pcm_manager.odl";
static const char* odl_mock_dummyplugin = "mock_dummyplugin.odl";

int test_setup(UNUSED void** state) {
    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    test_register_dummy_be();

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    amxo_connection_add(&parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    // Register data model
    amxb_register(bus_ctx, &dm);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_dummyplugin, root_obj), 0);

    assert_int_equal(_main(AMXO_START, &dm, &parser), 0);
    app_start(NULL, NULL, NULL);

    test_helper_handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    fprintf(stderr, "TEST-TEARDOWN-ENTER: %s\n", __func__);

    assert_int_equal(_main(AMXO_STOP, &dm, &parser), 0);

    assert_true(test_helper_was_unregister_called());
    test_helper_call_clear();

    amxo_resolver_import_close_all();
    assert_int_equal(test_unregister_dummy_be(), 0);
    amxd_dm_clean(&dm);
    amxo_parser_clean(&parser);

    fprintf(stderr, "TEST-TEARDOWN-EXIT: %s\n", __func__);
    return 0;
}

void test_wait_for_register(UNUSED void** state) {

    assert_int_equal(amxo_parser_parse_file(&parser, odl_mock_pcm_manager, root_obj), 0);
    amxp_signal_t* signal = NULL;
    signal = amxp_sigmngr_find_signal(NULL, "wait:PersistentConfiguration.");
    assert_non_null(signal);
    assert_false(test_helper_was_register_called());
    amxp_signal_emit(signal, NULL);
    test_helper_handle_events();
    assert_true(test_helper_was_register_called());
}