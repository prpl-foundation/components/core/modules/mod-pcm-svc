/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include "../common/test_helper.h"
#include "test_basic_integration.h"
#include "mod_pcm_svc.h"
#include "string.h"

#include <stdlib.h>
#include <stdio.h>

typedef enum {
    action_add_inst = 0,
    action_add_param,
    action_del_inst
} trans_action_t;

static void pcm_trans_action(trans_action_t action, const char* name, const char* path, const char* value) {
    amxd_object_t* object = amxd_dm_findf(test_helper_get_dm(), path);
    assert_non_null(object);

    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    amxd_trans_select_object(&transaction, object);

    switch(action) {
    case action_add_inst:
        amxd_trans_add_inst(&transaction, 0, name);
        break;

    case action_add_param:
        amxd_trans_set_value(cstring_t, &transaction, name, value);
        break;

    case action_del_inst:
        amxd_trans_del_inst(&transaction, 0, name);
        break;
    default:
        assert_true(0 && "Action has not specified");
        break;
    }

    assert_int_equal(amxd_trans_apply(&transaction, test_helper_get_dm()), amxd_status_ok);
    test_helper_handle_events();
    amxd_trans_clean(&transaction);
}

int test_setup(UNUSED void** state) {
    test_helper_setup("../common/basic_integration.odl", true);
    return 0;
}

int test_teardown(UNUSED void** state) {
    test_helper_teardown();
    return 0;
}

/**
 * Check UPC flagged items
 */

void test_mod_export_import_faild(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);

    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(dummy_object);

    amxc_var_t ret, args_exp, args_imp;
    amxc_var_init(&ret);
    amxc_var_init(&args_exp);
    amxc_var_init(&args_imp);

    amxc_var_set_type(&args_exp, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&args_imp, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, &args_exp, "usersetting", false);
    amxc_var_add_key(bool, &args_exp, "changed", false);
    set_export_to_fail(true);
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args_exp, &ret), amxd_status_unknown_error);

    /* create data struct */
    amxc_var_t* data = amxc_var_add_key(amxc_htable_t, &args_imp, "data", NULL);
    amxc_var_add_key(cstring_t, data, "type", "upc");
    amxc_var_add_key(cstring_t, data, "version", "1.0");

    amxc_var_t* add = amxc_var_add_key(amxc_htable_t, data, "add", NULL);
    amxc_var_t* delete = amxc_var_add_key(amxc_htable_t, data, "delete", NULL);
    amxc_var_t* set = amxc_var_add_key(amxc_htable_t, data, "set", NULL);
    amxc_var_t* hgwconfig = amxc_var_add_key(amxc_htable_t, set, "hgwconfig", NULL);
    amxc_var_t* dummyservice = amxc_var_add_key(amxc_htable_t, hgwconfig, "DummyService", NULL);
    amxc_var_t* multiinstance = amxc_var_add_key(amxc_htable_t, dummyservice, "InnerPersistentObjectMultiInstance", NULL);


    set_import_to_fail(true);
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmImport", &args_imp, &ret), amxd_status_unknown_error);

    amxc_var_clean(&ret);
    amxc_var_clean(&args_exp);
    amxc_var_clean(&args_imp);

}

void test_mod_export_persistent(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);

    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(dummy_object);

    amxc_var_t ret, args;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, &args, "usersetting", false);
    amxc_var_add_key(bool, &args, "changed", false);
    amxc_var_add_key(cstring_t, &args, "Flags", "upc,usersetting");
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);

    fprintf(stderr, "TEST-DEBUG: var dump: (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);

    amxc_var_t* data = amxc_var_get_key(&args, "data", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(data);
    amxc_var_t* add = amxc_var_get_key(data, "add", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(add);
    amxc_var_t* delete = amxc_var_get_key(data, "delete", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(delete);
    amxc_var_t* set = amxc_var_get_key(data, "set", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(set);

    amxc_var_t* hgwconfig = amxc_var_get_key(set, "hgwconfig", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(hgwconfig);
    amxc_var_t* dummyservice = amxc_var_get_key(hgwconfig, "DummyService", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(dummyservice);

    assert_string_equal(GETP_CHAR(dummyservice, "Name"), "DummyName");
    assert_null(amxc_var_get_key(dummyservice, "NonPersItem", AMXC_VAR_FLAG_DEFAULT));

    amxc_var_t* innerpersistentobject = amxc_var_get_key(dummyservice, "InnerPersistentObject", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(innerpersistentobject);

    assert_string_equal(GETP_CHAR(innerpersistentobject, "InnerName"), "InnerName");
    assert_null(amxc_var_get_key(innerpersistentobject, "InnerStatus", AMXC_VAR_FLAG_DEFAULT));
    assert_null(amxc_var_get_key(innerpersistentobject, "InnerNonPersItem", AMXC_VAR_FLAG_DEFAULT));

    amxc_var_t* innerpersistentobjectnotflagged = amxc_var_get_key(dummyservice, "InnerPersistentObjectNotFlagged", AMXC_VAR_FLAG_DEFAULT);
    assert_null(innerpersistentobjectnotflagged);

    amxc_var_t* innerpersistentobjectmultiinstance = amxc_var_get_key(dummyservice, "InnerPersistentObjectMultiInstance", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(innerpersistentobjectmultiinstance);

    amxc_var_t* instance = pcm_get_element(innerpersistentobjectmultiinstance, "cpe-1");
    assert_non_null(instance);
    assert_string_equal(GETP_CHAR(instance, "InnerNameMultiInstance"), "Instance_1");
    assert_string_equal(GETP_CHAR(instance, "InnerStatusMultiInstance"), "OK");

    instance = pcm_get_element(innerpersistentobjectmultiinstance, "cpe-2");
    assert_non_null(instance);
    assert_string_equal(GETP_CHAR(instance, "InnerNameMultiInstance"), "Instance_2");
    assert_string_equal(GETP_CHAR(instance, "InnerStatusMultiInstance"), "OK");

    assert_null(amxc_var_get_key(dummyservice, "InnerNonPersistentObject", AMXC_VAR_FLAG_DEFAULT));

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}

void test_mod_export_persistent_new_flags(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);

    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(dummy_object);

    amxc_var_t ret, args;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, &args, "usersetting", false);
    amxc_var_add_key(bool, &args, "changed", false);
    amxc_var_add_key(cstring_t, &args, "Flags", "wifisetting");
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);

    fprintf(stderr, "TEST-DEBUG: var dump: (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);

    amxc_var_t* data = amxc_var_get_key(&args, "data", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(data);
    amxc_var_t* add = amxc_var_get_key(data, "add", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(add);
    amxc_var_t* delete = amxc_var_get_key(data, "delete", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(delete);
    amxc_var_t* set = amxc_var_get_key(data, "set", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(set);

    amxc_var_t* hgwconfig = amxc_var_get_key(set, "hgwconfig", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(hgwconfig);
    amxc_var_t* dummyservice = amxc_var_get_key(hgwconfig, "DummyService", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(dummyservice);

    assert_string_equal(GETP_CHAR(dummyservice, "WiFiSetting"), "DummyValue");
    assert_string_equal(GETP_CHAR(dummyservice, "UserWiFiSetting"), "DummyValue");
    assert_null(amxc_var_get_key(dummyservice, "Name", AMXC_VAR_FLAG_DEFAULT));

    amxc_var_t* innerpersistentobject = amxc_var_get_key(dummyservice, "InnerPersistentObject", AMXC_VAR_FLAG_DEFAULT);
    assert_null(innerpersistentobject);


    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}

/**
 * Change the datamodel
 */
void test_mod_upc_change(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);

    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(dummy_object);


    /* rename Name in the datamodel (externally : using a transaction) */
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    amxd_trans_select_object(&transaction, dummy_object);
    amxd_trans_set_value(cstring_t, &transaction, "Name", "TransName");
    assert_int_equal(amxd_trans_apply(&transaction, test_helper_get_dm()), amxd_status_ok);
    test_helper_handle_events();
    amxd_trans_clean(&transaction);

    /* check or Name is properly set in the datamodel */
    amxd_object_t* object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(object);
    char* object_param = amxd_object_get_cstring_t(object, "Name", NULL);
    assert_string_equal(object_param, "TransName");
    free(object_param);

    /* do the export and check or the change is part of the export */
    amxc_var_t ret;
    amxc_var_init(&ret);

    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);

    fprintf(stderr, "TEST-DEBUG: var dump: (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);

    amxc_var_t* data = amxc_var_get_key(&args, "data", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(data);
    amxc_var_t* add = amxc_var_get_key(data, "add", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(add);
    amxc_var_t* delete = amxc_var_get_key(data, "delete", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(delete);
    amxc_var_t* set = amxc_var_get_key(data, "set", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(set);

    amxc_var_t* hgwconfig = amxc_var_get_key(set, "hgwconfig", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(hgwconfig);
    amxc_var_t* dummyservice = amxc_var_get_key(hgwconfig, "DummyService", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(dummyservice);

    assert_string_equal(GETP_CHAR(dummyservice, "Name"), "TransName");


    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}

void test_mod_default_import(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");

    amxc_var_t ret;
    amxc_var_init(&ret);
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    /* create data struct */
    amxc_var_t* data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "type", "upc");
    amxc_var_add_key(cstring_t, data, "version", "1.0");

    amxc_var_t* add = amxc_var_add_key(amxc_htable_t, data, "add", NULL);
    amxc_var_t* delete = amxc_var_add_key(amxc_htable_t, data, "delete", NULL);
    amxc_var_t* set = amxc_var_add_key(amxc_htable_t, data, "set", NULL);
    amxc_var_t* hgwconfig = amxc_var_add_key(amxc_htable_t, set, "hgwconfig", NULL);
    amxc_var_t* dummyservice = amxc_var_add_key(amxc_htable_t, hgwconfig, "DummyService", NULL);
    amxc_var_t* multiinstance = amxc_var_add_key(amxc_htable_t, dummyservice, "InnerPersistentObjectMultiInstance", NULL);

    /* rename Name parameter */
    amxc_var_add_key(cstring_t, dummyservice, "Name", "NewName");

    /* rename multi-instance (InnerNameMultiInstance and InnerStatusMultiInstance) parameter */
    amxc_var_t* instance = amxc_var_add_key(amxc_htable_t, multiinstance, "cpe-1", NULL);
    amxc_var_add_key(cstring_t, instance, "InnerNameMultiInstance", "NewName");
    amxc_var_add_key(cstring_t, instance, "InnerStatusMultiInstance", "NewStatus");

    /* create new instance*/
    instance = amxc_var_add_key(amxc_htable_t, multiinstance, "cpe-3", NULL);
    amxc_var_add_key(cstring_t, instance, "InnerNameMultiInstance", "NewName-3");
    amxc_var_add_key(cstring_t, instance, "InnerStatusMultiInstance", "NewStatus-3");
    amxc_var_dump(&args, 0);

    /* call import function */
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmImport", &args, &ret), amxd_status_ok);

    /* handle events caused by the import */
    test_helper_handle_events();

    /* check or data has been properly been imported into the datamodel */
    amxd_object_t* object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(object);
    char* object_param = amxd_object_get_cstring_t(object, "Name", NULL);
    assert_string_equal(object_param, "NewName");
    free(object_param);

    object = amxd_dm_findf(test_helper_get_dm(), "DummyService.InnerPersistentObjectMultiInstance.cpe-1.");
    assert_non_null(object);
    object_param = amxd_object_get_cstring_t(object, "InnerNameMultiInstance", NULL);
    assert_string_equal(object_param, "NewName");
    free(object_param);

    object_param = amxd_object_get_cstring_t(object, "InnerStatusMultiInstance", NULL);
    assert_string_equal(object_param, "NewStatus");
    free(object_param);

    object = amxd_dm_findf(test_helper_get_dm(), "DummyService.InnerPersistentObjectMultiInstance.cpe-3.");
    assert_non_null(object);
    object_param = amxd_object_get_cstring_t(object, "InnerNameMultiInstance", NULL);
    assert_string_equal(object_param, "NewName-3");
    free(object_param);

    object_param = amxd_object_get_cstring_t(object, "InnerStatusMultiInstance", NULL);
    assert_string_equal(object_param, "NewStatus-3");
    free(object_param);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}


void test_import_by_index(void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    char* object_param = NULL;
    amxc_var_t ret;
    amxc_var_t args;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    /* create data struct */
    amxc_var_t* data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "type", "upc");
    amxc_var_add_key(cstring_t, data, "version", "1.0");

    amxc_var_t* add = amxc_var_add_key(amxc_htable_t, data, "add", NULL);
    amxc_var_t* delete = amxc_var_add_key(amxc_htable_t, data, "delete", NULL);
    amxc_var_t* set = amxc_var_add_key(amxc_htable_t, data, "set", NULL);
    amxc_var_t* hgwconfig = amxc_var_add_key(amxc_htable_t, set, "hgwconfig", NULL);
    amxc_var_t* dummyservice = amxc_var_add_key(amxc_htable_t, hgwconfig, "DummyService", NULL);
    amxc_var_t* multiinstance = amxc_var_add_key(amxc_htable_t, dummyservice, "IndexMultiInstance", NULL);

    /* rename multi-instance (InnerNameMultiInstance and InnerStatusMultiInstance) parameter */
    amxc_var_t* instance = amxc_var_add_key(amxc_htable_t, multiinstance, "1", NULL);
    amxc_var_add_key(cstring_t, instance, "InnerNameMultiInstance", "NewName-1");
    amxc_var_add_key(cstring_t, instance, "InnerStatusMultiInstance", "NewStatus-1");

    /* create new instance*/
    instance = amxc_var_add_key(amxc_htable_t, multiinstance, "2", NULL);
    amxc_var_add_key(cstring_t, instance, "InnerNameMultiInstance", "NewName-2");
    amxc_var_add_key(cstring_t, instance, "InnerStatusMultiInstance", "NewStatus-2");
    amxc_var_dump(&args, 0);

    // /* call import function */
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmImport", &args, &ret), amxd_status_ok);

    /* handle events caused by the import */
    test_helper_handle_events();

    /* check or data has been properly been imported into the datamodel */
    amxd_object_t* object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(object);

    object = amxd_dm_findf(test_helper_get_dm(), "DummyService.IndexMultiInstance.1.");
    assert_non_null(object);
    object_param = amxd_object_get_cstring_t(object, "InnerNameMultiInstance", NULL);
    assert_string_equal(object_param, "NewName-1");
    free(object_param);

    object_param = amxd_object_get_cstring_t(object, "InnerStatusMultiInstance", NULL);
    assert_string_equal(object_param, "NewStatus-1");
    free(object_param);

    object = amxd_dm_findf(test_helper_get_dm(), "DummyService.IndexMultiInstance.2.");
    assert_non_null(object);
    object_param = amxd_object_get_cstring_t(object, "InnerNameMultiInstance", NULL);
    assert_string_equal(object_param, "NewName-2");
    free(object_param);

    object_param = amxd_object_get_cstring_t(object, "InnerStatusMultiInstance", NULL);
    assert_string_equal(object_param, "NewStatus-2");
    free(object_param);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    fprintf(stderr, "TEST-EXIT: %s\n", __func__);

}

void test_ref_parameter(void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    amxc_var_t ret;
    amxc_var_t args;
    UNUSED uint32_t ref_path_index = 0;
    char* ref_path = NULL;
    amxd_object_t* inst_object = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(dummy_object);

    pcm_trans_action(action_add_inst, "test_1", "DummyService.InnerPersistentObjectMultiInstance.", NULL);

    inst_object = amxd_dm_findf(test_helper_get_dm(), "DummyService.InnerPersistentObjectMultiInstance.test_1");
    assert_non_null(inst_object);
    ref_path_index = amxd_object_get_index(inst_object);
    ref_path = amxd_object_get_path(inst_object, 1);
    printf("Before backup the ref_path is: %s\n", ref_path);

    pcm_trans_action(action_add_param, "RefParameter", "DummyService.", ref_path);
    free(ref_path);

    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);
    const char* backup_ref_param = GETP_CHAR(&args, "data.set.hgwconfig.DummyService.RefParameter");

    pcm_trans_action(action_del_inst, "test_1", "DummyService.InnerPersistentObjectMultiInstance.", NULL);
    pcm_trans_action(action_add_inst, "test_3", "DummyService.InnerPersistentObjectMultiInstance.", NULL);
    pcm_trans_action(action_add_inst, "test_2", "DummyService.InnerPersistentObjectMultiInstance.", NULL);
    pcm_trans_action(action_add_inst, "test_1", "DummyService.InnerPersistentObjectMultiInstance.", NULL);

    inst_object = amxd_dm_findf(test_helper_get_dm(), "%s", backup_ref_param);
    assert_non_null(inst_object);

    ref_path = amxd_object_get_path(inst_object, 1);
    printf("In backup file the ref_path is: %s\n", backup_ref_param);
    printf("After restore the ref_path is: %s\n", ref_path);
    free(ref_path);
    assert_int_equal(ref_path_index + 3, amxd_object_get_index(inst_object));

    amxc_var_clean(&ret);
    amxc_var_clean(&args);
}