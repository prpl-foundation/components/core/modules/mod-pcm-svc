/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include "../common/test_helper.h"
#include "test_delete_instances.h"
#include "mod_pcm_inst.h"
#include "mod_pcm_svc.h"

#include <stdio.h>
#include <stdlib.h>

#define COPT_NAME "name"

int test_setup(UNUSED void** state) {
    test_helper_setup("../common/delete_instances.odl", true);
    return 0;
}

int test_teardown(UNUSED void** state) {
    test_helper_teardown();
    return 0;
}


void test_delete_instance_without_primkey(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(dummy_object);

    /* print the export result before the delete */
    amxc_var_t ret;
    amxc_var_init(&ret);
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);
    fprintf(stderr, "TEST-DEBUG: export-args before: (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    /* remove the instance in the datamodel (externally : using a transaction) */
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    assert_int_equal(amxd_trans_select_pathf(&transaction, "DummyService.Template"), 0);
    assert_int_equal(amxd_trans_del_inst(&transaction, 0, "cpe-1"), 0);
    fprintf(stderr, "TEST-DEBUG: transaction: (%s)\n", __func__);
    amxd_trans_dump(&transaction, STDERR_FILENO, false);
    assert_int_equal(amxd_trans_apply(&transaction, test_helper_get_dm()), amxd_status_ok);
    test_helper_handle_events();
    amxd_trans_clean(&transaction);

    /* print the export result after the delete */
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);
    fprintf(stderr, "TEST-DEBUG: export-args after: (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);

    /* check or the export data is what is expected */
    amxc_var_t* data = amxc_var_get_key(&args, "data", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(data);
    amxc_var_t* add = amxc_var_get_key(data, "add", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(add);
    amxc_var_t* delete = amxc_var_get_key(data, "delete", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(delete);
    amxc_var_t* set = amxc_var_get_key(data, "set", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(set);

    amxc_var_t* hgwconfig = amxc_var_get_key(delete, "hgwconfig", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(hgwconfig);
    amxc_var_t* dummyservice = amxc_var_get_key(hgwconfig, "DummyService", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(dummyservice);
    amxc_var_t* template = amxc_var_get_key(dummyservice, "Template", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(template);
    amxc_var_t* instance = pcm_get_element(template, "cpe-1");
    assert_non_null(instance);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    /* validate the deleted file that should be generated */
    amxc_var_t deleted_instances;
    amxc_var_init(&deleted_instances);
    amxc_var_set_type(&deleted_instances, AMXC_VAR_ID_LIST);
    pcm_inst_read(test_helper_get_delfilepath(), &deleted_instances);
    fprintf(stderr, "TEST-DEBUG: deleted_instances: (%s)\n", __func__);
    amxc_var_dump(&deleted_instances, STDERR_FILENO);
    amxc_var_t* firstitem = amxc_var_get_first(&deleted_instances);
    assert_string_equal(GETP_CHAR(firstitem, "name"), "cpe-1");
    assert_string_equal(GETP_CHAR(firstitem, "path"), "DummyService.Template.");
    amxc_var_clean(&deleted_instances);

    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}

void test_readd_instance_without_primkey(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(dummy_object);

    /* print the export result before the delete */
    amxc_var_t ret;
    amxc_var_init(&ret);
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);
    fprintf(stderr, "TEST-DEBUG: export-args before add : (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    assert_int_equal(amxd_trans_select_pathf(&transaction, "DummyService.Template"), 0);
    assert_int_equal(amxd_trans_add_inst(&transaction, 0, "cpe-1"), 0);
    fprintf(stderr, "TEST-DEBUG: transaction: (%s)\n", __func__);
    amxd_trans_dump(&transaction, STDERR_FILENO, false);
    assert_int_equal(amxd_trans_apply(&transaction, test_helper_get_dm()), amxd_status_ok);
    test_helper_handle_events();
    amxd_trans_clean(&transaction);

    /* print the export result after the delete */
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);
    fprintf(stderr, "TEST-DEBUG: export-args after add: (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);

    /* check or the export data is what is expected */
    amxc_var_t* data = amxc_var_get_key(&args, "data", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(data);
    amxc_var_t* add = amxc_var_get_key(data, "add", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(add);
    amxc_var_t* delete = amxc_var_get_key(data, "delete", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(delete);
    amxc_var_t* set = amxc_var_get_key(data, "set", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(set);

    amxc_var_t* hgwconfig = amxc_var_get_key(set, "hgwconfig", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(hgwconfig);
    amxc_var_t* dummyservice = amxc_var_get_key(hgwconfig, "DummyService", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(dummyservice);
    amxc_var_t* template = amxc_var_get_key(dummyservice, "Template", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(template);
    amxc_var_t* instance = amxc_var_get_key(template, "cpe-1", AMXC_VAR_FLAG_DEFAULT);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    /* validate the deleted file should be empty */
    amxc_var_t deleted_instances;
    amxc_var_init(&deleted_instances);
    amxc_var_set_type(&deleted_instances, AMXC_VAR_ID_LIST);
    pcm_inst_read(test_helper_get_delfilepath(), &deleted_instances);
    fprintf(stderr, "TEST-DEBUG: deleted_instances: (%s)\n", __func__);
    amxc_var_dump(&deleted_instances, STDERR_FILENO);
    amxc_var_t* firstitem = amxc_var_get_first(&deleted_instances);
    assert_null(firstitem);
    amxc_var_clean(&deleted_instances);

    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}

void test_delete_instance_with_primkey(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(dummy_object);

    /* print the export result before the delete */
    amxc_var_t ret;
    amxc_var_init(&ret);
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);
    fprintf(stderr, "TEST-DEBUG: export-args before: (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    /* remove the instance in the datamodel (externally : using a transaction) */
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    assert_int_equal(amxd_trans_select_pathf(&transaction, "DummyService.Template2"), 0);
    assert_int_equal(amxd_trans_del_inst(&transaction, 0, "cpe-2"), 0);
    fprintf(stderr, "TEST-DEBUG: transaction: (%s)\n", __func__);
    amxd_trans_dump(&transaction, STDERR_FILENO, false);
    assert_int_equal(amxd_trans_apply(&transaction, test_helper_get_dm()), amxd_status_ok);
    test_helper_handle_events();
    amxd_trans_clean(&transaction);

    /* print the export result after the delete */
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);
    fprintf(stderr, "TEST-DEBUG: export-args after: (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);

    /* check or the export data is what is expected */
    amxc_var_t* data = amxc_var_get_key(&args, "data", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(data);
    amxc_var_t* add = amxc_var_get_key(data, "add", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(add);
    amxc_var_t* delete = amxc_var_get_key(data, "delete", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(delete);
    amxc_var_t* set = amxc_var_get_key(data, "set", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(set);

    amxc_var_t* hgwconfig = amxc_var_get_key(delete, "hgwconfig", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(hgwconfig);
    amxc_var_t* dummyservice = amxc_var_get_key(hgwconfig, "DummyService", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(dummyservice);
    amxc_var_t* template = amxc_var_get_key(dummyservice, "Template2", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(template);
    amxc_var_t* instance = pcm_get_element(template, "cpe-2");
    assert_non_null(instance);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    /* validate the deleted file that should be generated */
    amxc_var_t deleted_instances;
    amxc_var_init(&deleted_instances);
    amxc_var_set_type(&deleted_instances, AMXC_VAR_ID_LIST);
    pcm_inst_read(test_helper_get_delfilepath(), &deleted_instances);
    fprintf(stderr, "TEST-DEBUG: deleted_instances: (%s)\n", __func__);
    amxc_var_dump(&deleted_instances, STDERR_FILENO);
    amxc_var_t* firstitem = amxc_var_get_first(&deleted_instances);
    assert_string_equal(GETP_CHAR(firstitem, "name"), "cpe-2");
    assert_string_equal(GETP_CHAR(firstitem, "path"), "DummyService.Template2.");
    amxc_var_clean(&deleted_instances);

    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}

void test_readd_instance_with_primkey(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");
    assert_non_null(dummy_object);

    /* print the export result before the delete */
    amxc_var_t ret;
    amxc_var_init(&ret);
    amxc_var_t args;
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);
    fprintf(stderr, "TEST-DEBUG: export-args before add : (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    assert_int_equal(amxd_trans_select_pathf(&transaction, "DummyService.Template2"), 0);
    assert_int_equal(amxd_trans_add_inst(&transaction, 0, "cpe-2"), 0);
    fprintf(stderr, "TEST-DEBUG: transaction: (%s)\n", __func__);
    amxd_trans_dump(&transaction, STDERR_FILENO, false);
    assert_int_equal(amxd_trans_apply(&transaction, test_helper_get_dm()), amxd_status_ok);
    test_helper_handle_events();
    amxd_trans_clean(&transaction);

    /* print the export result after the delete */
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmExport", &args, &ret), amxd_status_ok);
    fprintf(stderr, "TEST-DEBUG: export-args after add: (%s)\n", __func__);
    amxc_var_dump(&args, STDERR_FILENO);

    /* check or the export data is what is expected */
    amxc_var_t* data = amxc_var_get_key(&args, "data", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(data);
    amxc_var_t* add = amxc_var_get_key(data, "add", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(add);
    amxc_var_t* delete = amxc_var_get_key(data, "delete", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(delete);
    amxc_var_t* set = amxc_var_get_key(data, "set", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(set);

    amxc_var_t* hgwconfig = amxc_var_get_key(set, "hgwconfig", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(hgwconfig);
    amxc_var_t* dummyservice = amxc_var_get_key(hgwconfig, "DummyService", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(dummyservice);
    amxc_var_t* template = amxc_var_get_key(dummyservice, "Template2", AMXC_VAR_FLAG_DEFAULT);
    assert_non_null(template);
    amxc_var_t* instance = amxc_var_get_key(template, "cpe-2", AMXC_VAR_FLAG_DEFAULT);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    /* validate the deleted file should be empty */
    amxc_var_t deleted_instances;
    amxc_var_init(&deleted_instances);
    amxc_var_set_type(&deleted_instances, AMXC_VAR_ID_LIST);
    pcm_inst_read(test_helper_get_delfilepath(), &deleted_instances);
    fprintf(stderr, "TEST-DEBUG: deleted_instances: (%s)\n", __func__);
    amxc_var_dump(&deleted_instances, STDERR_FILENO);
    amxc_var_t* firstitem = amxc_var_get_first(&deleted_instances);
    assert_null(firstitem);
    amxc_var_clean(&deleted_instances);

    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}

void test_delete_instance_with_import(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    amxd_object_t* dummy_object = amxd_dm_findf(test_helper_get_dm(), "DummyService");

    amxc_var_t ret, args;
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    /* create data struct do delete cpe-1 instance */
    amxc_var_t* data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    amxc_var_add_key(cstring_t, data, "type", "upc");
    amxc_var_add_key(cstring_t, data, "version", "1.0");

    amxc_var_t* add = amxc_var_add_key(amxc_htable_t, data, "add", NULL);
    amxc_var_t* delete = amxc_var_add_key(amxc_htable_t, data, "delete", NULL);
    amxc_var_t* set = amxc_var_add_key(amxc_htable_t, data, "set", NULL);
    amxc_var_t* hgwconfig = amxc_var_add_key(amxc_htable_t, delete, "hgwconfig", NULL);
    amxc_var_t* dummyservice = amxc_var_add_key(amxc_htable_t, hgwconfig, "DummyService", NULL);
    amxc_var_t* template = amxc_var_add_key(amxc_llist_t, dummyservice, "Template", NULL);
    amxc_var_t* entry = amxc_var_add(amxc_htable_t, template, NULL);
    amxc_var_t* instance = amxc_var_add_key(amxc_htable_t, entry, "cpe-2", NULL);
    amxc_var_add_key(cstring_t, instance, "Alias", "cpe-2");

    /* display args */
    amxc_var_dump(&args, STDERR_FILENO);

    /* call import function */
    assert_int_equal(amxd_object_invoke_function(dummy_object, "PcmImport", &args, &ret), amxd_status_ok);
    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    /* handle events caused by the import (normally done by amxrt) */
    test_helper_handle_events();

    /* check or delete instance has been removed from datamodel */
    amxd_object_t* object = amxd_dm_findf(test_helper_get_dm(), "DummyService.Template.");
    assert_non_null(object);
    object = amxd_dm_findf(test_helper_get_dm(), "DummyService.Template.cpe-2.");
    assert_null(object);

    /* validate the deleted file */
    amxc_var_t deleted_instances;
    amxc_var_init(&deleted_instances);
    amxc_var_set_type(&deleted_instances, AMXC_VAR_ID_LIST);
    pcm_inst_read(test_helper_get_delfilepath(), &deleted_instances);
    fprintf(stderr, "TEST-DEBUG: deleted_instances: (%s)\n", __func__);
    amxc_var_dump(&deleted_instances, STDERR_FILENO);
    amxc_var_t* firstitem = amxc_var_get_first(&deleted_instances);
    assert_string_equal(GETP_CHAR(firstitem, "name"), "cpe-2");
    assert_string_equal(GETP_CHAR(firstitem, "path"), "DummyService.Template.");
    amxc_var_clean(&deleted_instances);

    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}
