# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.13.7 - 2024-12-16(15:26:58 +0000)

### Fixes

- Reboot data model is not upgrade persistent

## Release v0.13.6 - 2024-09-11(07:32:42 +0000)

### Fixes

- [mod-pcm-svc] Enable setting private and readonly params

## Release v0.13.5 - 2024-09-05(11:30:47 +0000)

### Other

- - mod_pcm log spam at each scan

## Release v0.13.4 - 2024-08-28(08:09:53 +0000)

### Other

- [CHR2fA] wrong AccessControl rule config after upgrade from DEL13 to DEL14

## Release v0.13.3 - 2024-07-01(10:05:29 +0000)

### Fixes

- [mod-pcm-svc] cannot read protected parameters of PCM plugin

## Release v0.13.2 - 2024-06-12(06:22:13 +0000)

### Other

- Device.ManagementServer.Subscription is empty

## Release v0.13.1 - 2024-06-06(09:22:29 +0000)

### Other

- Device.ManagementServer.Subscription is empty

## Release v0.13.0 - 2024-05-02(08:15:51 +0000)

### New

- [mod-pcm-svc] Enhance the PcmImport functionality

## Release v0.12.0 - 2024-04-16(11:02:14 +0000)

### New

- [tr181-pcm] Userflags must be configurable

## Release v0.11.1 - 2024-03-21(09:05:35 +0000)

### Fixes

- After reboot all hosts are disconnected (AKA amb timeouts)

## Release v0.11.0 - 2024-02-28(10:26:17 +0000)

### New

- [tr181-pcm] Rework the Backup/Restore API

## Release v0.10.0 - 2024-02-23(09:32:02 +0000)

### Other

- Also restore parameters if they are read-only

## Release v0.9.4 - 2024-02-12(09:50:29 +0000)

### Other

- [mod-pcm-svc] Register default Import/Export function unconditionally

## Release v0.9.3 - 2024-01-23(14:10:56 +0000)

### Fixes

- [PCM] The BackupStatus takes the value "PartialSuccess" instead of "Success"

## Release v0.9.2 - 2024-01-15(08:04:42 +0000)

### Fixes

- [PCM] Duplicate instance error when restarting the pcm-manager

## Release v0.9.1 - 2024-01-08(08:39:41 +0000)

### Fixes

- [SAFRAN_PRPL] Administration password is not upgrade persistent

## Release v0.9.0 - 2023-11-23(13:24:52 +0000)

### New

- [mod_pcm]unable to export parameter that have an action read 'hide_value'

## Release v0.8.0 - 2023-11-23(12:58:35 +0000)

### New

- [DHCPv4][PCM] Device leases MUST be upgrade persistent

## Release v0.7.2 - 2023-11-23(12:48:57 +0000)

### Fixes

- [PCM] The pcm-manager DM isn't updated when a service is unregistered

## Release v0.7.1 - 2023-11-08(15:32:12 +0000)

### Other

- Restore opensource mirror

## Release v0.7.0 - 2023-09-28(09:36:30 +0000)

### New

- [PCM] [JSON format] Clean up the existing json format

## Release v0.6.2 - 2023-09-22(11:16:00 +0000)

### Fixes

- Crash during shutdown

## Release v0.6.1 - 2023-09-07(15:21:50 +0000)

### Fixes

- [tr181-pcm] Objects are not registered with pcm-manager

## Release v0.6.0 - 2023-08-31(13:52:46 +0000)

### New

- Move datamodel prefixes to the device proxy

## Release v0.5.3 - 2023-08-26(06:27:00 +0000)

### Other

- mod-pcm-svc should handle properly the case where the root object is not persistent

## Release v0.5.2 - 2023-06-16(10:11:56 +0000)

### Fixes

- Issue: HOP-3724[mod_pcm_svc] PcmExport and PcmImport must be protected functions

## Release v0.5.1 - 2023-06-15(08:32:43 +0000)

### Fixes

- [tr181-pcm] Restore function has stop working

## Release v0.5.0 - 2023-05-05(13:59:15 +0000)

### New

- [mod-pcm-svc] Cleaning up the code

## Release v0.4.0 - 2023-03-20(11:26:01 +0000)

### New

- [mod-pcm-svc] Migrate the mod_bart_svc to the mod_pcm_svc module

## Release v0.3.5 - 2023-02-16(10:37:19 +0000)

### Other

- [mod-pcm-svc] generated datamodel documentation is empty

## Release v0.3.4 - 2022-12-03(08:47:13 +0000)

### Fixes

- [TR181 PCM] pcm-manager crashes after firstboot

## Release v0.3.3 - 2022-11-07(12:21:04 +0000)

### Fixes

- gcc 11.2.0 linker cannot find -lsahtrace

## Release v0.3.2 - 2022-08-17(09:22:29 +0000)

### Fixes

- [PRPL DISH][DataModel] DHCPv4.Server.Pool.1.Client.1.IPv4Address.1.IPAddress doesn't get the new IP address

## Release v0.3.1 - 2022-05-20(06:35:52 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.3.0 - 2022-02-03(16:52:32 +0000)

### New

- mod_pcm_svc shout support restore on register and auto sync ctrl

## Release v0.2.1 - 2022-01-03(09:46:42 +0000)

### Fixes

- mod_pcm_svc should not use printfs

## Release v0.2.0 - 2021-12-15(09:32:20 +0000)

### New

- Add multi object support to mod_pcm_svc

## Release v0.1.1 - 2021-12-09(13:22:20 +0000)

### Other

- Add missing libsahtrace dependency
- Change name to mod-pcm-svc
- Add custom flags support to mod_pcm_svc

## Release v0.1.0 - 2021-12-03(12:06:30 +0000)

### New

- PCM integration module implementation

