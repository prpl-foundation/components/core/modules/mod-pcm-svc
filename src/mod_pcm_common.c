/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_pcm_def.h"

#define ME "mod_pcm_svc"
#define PCM_VERSION_MAJOR      1
#define PCM_VERSION_MINOR      0

bool pcm_version_get(char* string, size_t size) {
    bool ret = false;
    size_t version = 0;

    when_null_trace(string, exit, ERROR, "Invalid argument");

    version = (size_t) snprintf(string, size, "%u.%u", PCM_VERSION_MAJOR, PCM_VERSION_MINOR);
    when_true_trace(version >= size, exit, ERROR, "Version string truncated");
    ret = true;
exit:
    return ret;
}

bool pcm_version_parse(const char* version, unsigned int* major, unsigned int* minor) {
    bool retval = false;
    char* v = NULL;
    char* s = NULL;
    char* endptr = NULL;

    when_null_trace(version, error, ERROR, "Invalid argument");
    when_null_trace(major, error, ERROR, "Invalid argument");
    when_null_trace(minor, error, ERROR, "Invalid argument");

    v = strdup(version);
    when_null_trace(v, error, ERROR, "Out of memory");

    s = strchr(v, '.');
    when_null_trace(s, error, ERROR, "No separator in version '%s'", version);
    *s = 0;

    *major = strtoul(v, &endptr, 10);
    if(*endptr) {
        SAH_TRACEZ_ERROR(ME, "Invalid major number in version '%s'", version);
        goto error;
    }

    *minor = strtoul(s + 1, &endptr, 10);
    if(*endptr) {
        SAH_TRACEZ_ERROR(ME, "Invalid minor number in version '%s'", version);
        goto error;
    }

    retval = true;

error:
    free(v);
    return retval;
}

pcm_version_t pcm_version_verify(unsigned int major, unsigned int minor) {
    if(major > PCM_VERSION_MAJOR) {
        SAH_TRACEZ_ERROR(ME, "Major number [%u] greater than supported [%u]", major, PCM_VERSION_MAJOR);
        return pcm_version_unsupported;
    }

    if((major == PCM_VERSION_MAJOR) && (minor > PCM_VERSION_MINOR)) {
        SAH_TRACEZ_ERROR(ME, "Minor number [%u] greater than supported [%u]", minor, PCM_VERSION_MINOR);
        return pcm_version_supported_partial;
    }

    return pcm_version_supported;
}

int pcm_mk_path(const char* path, const mode_t mode) {
    int ret = -1;
    char* fp = NULL;
    when_str_empty(path, exit);

    fp = strdup(path);
    for(char* p = strchr(fp + 1, '/'); p; p = strchr(p + 1, '/')) {
        *p = '\0';
        if(mkdir(fp, mode) == -1) {
            if(errno != EEXIST) {
                *p = '/';
                goto exit;
            }
        }
        *p = '/';
    }

    ret = 0;
exit:
    free(fp);
    fp = NULL;
    return ret;
}
