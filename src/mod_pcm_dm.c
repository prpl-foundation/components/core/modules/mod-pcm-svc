/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_pcm_dm.h"
#include "mod_pcm_path.h"
#include "mod_pcm_svc.h"
#include "mod_pcm_inst.h"
#include "mod_pcm_ctx.h"

#define ME "mod_pcm_svc"
#define STR_EMPTY(x) (x) == NULL || (x)[0] == '\0'

static bool pcm_dm_parameter_selected(const amxd_param_t* param, bool changed);
static bool pcm_dm_for_each_deleted_instance(bool usersetting, pcm_path_t* path, dm_for_each_cb cb, void* userdata);
static bool pcm_dm_for_each_object(amxd_object_t* object, bool usersetting, bool changed, pcm_path_t* path, dm_for_each_cb cb, void* userdata);
static bool pcm_dm_key_is_index(const char* key);
static bool pcm_dm_is_upc_overwrite(amxd_object_t* root_obj, const pcm_path_t* path, int offset, const char* param_key);
static bool pcm_dm_is_instance_indicator(const char* param_key);
static void pcm_param_get_value(amxd_param_t* param, amxc_var_t* ret_value);
static void pcm_cnv_to_search_path(const char* src_path_str, pcm_path_t* ret_path);

amxd_object_t* pcm_dm_get_object(amxd_object_t* curr, const pcm_path_t* path, int offset, bool with_param) {
    int count = pcm_path_element_count(path);
    const char* elem_name = NULL;
    amxd_object_t* ret_obj = NULL;

    when_null_trace(curr, exit, ERROR, "The root object is not provided");
    when_null_trace(path, exit, ERROR, "The parameter or object path is not provided");
    when_true_trace(offset < 0, exit, ERROR, "Invalid offset argument [%d]", offset);
    when_true_trace(offset > count, exit, ERROR, "The offset is greater than the number of elements in the path");
    when_true_trace(count < 0, exit, ERROR, "Invalid path '%s'", pcm_path_da_get(path));
    when_true_status(count == offset, exit, ret_obj = curr);

    /* Path contains a parameter, ignore it */
    count = (with_param)? count - 1 : count;

    for(int i = offset; i < count; i++) {
        elem_name = pcm_path_element_da_get(path, i);
        when_str_empty_trace(elem_name, exit, ERROR, "Invalid element in path '%s'", pcm_path_da_get(path));

        ret_obj = amxd_object_findf(curr, "%s", elem_name);
        when_null_trace(ret_obj, exit, ERROR, "Object '%s' not found", elem_name);
        curr = ret_obj;
    }

exit:
    return ret_obj;
}

bool pcm_dm_add_inst(amxd_object_t* curr, const pcm_path_t* path, int offset, amxd_trans_t* trans) {
    int count = pcm_path_element_count(path);
    const char* elem_name = NULL;
    bool retval = false;
    amxd_object_t* obj = NULL;

    for(int i = offset; i < count; i++) {
        elem_name = pcm_path_element_da_get(path, i);
        when_str_empty_trace(elem_name, exit, ERROR, "Invalid element in path '%s'", pcm_path_da_get(path));

        obj = amxd_object_findf(curr, "%s", elem_name);

        if((obj == NULL) && (amxd_object_template == amxd_object_get_type(curr))) {
            when_failed_trace(amxd_trans_select_object(trans, curr), exit, ERROR,
                              "Could not select the object %s", amxd_object_get_name(curr, AMXD_OBJECT_NAMED));
            if(pcm_dm_key_is_index(elem_name)) {
                when_failed_trace(amxd_trans_add_inst(trans, atoi(elem_name), NULL), exit, ERROR,
                                  "Could not add instance with index %s", elem_name);
            } else {
                when_failed_trace(amxd_trans_add_inst(trans, 0, elem_name), exit, ERROR,
                                  "Could not add instance with Alias : %s", elem_name);
            }
        }
        curr = obj;
    }
    retval = true;
exit:
    return retval;
}

bool pcm_dm_set(amxd_object_t* object, const pcm_path_t* path, int offset, amxc_var_t* value, pcm_dataset_t dataset) {
    amxd_object_t* obj = NULL;
    amxd_trans_t transaction;
    bool retval = false;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_priv, true);

    if((object == NULL) || (path == NULL) || (value == NULL) || (dataset != pcm_dataset_set)) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument - object:[%p] path:[%p] value:[%p] dataset:[%d]", object, path, value, dataset);
        goto exit;
    }

    obj = pcm_dm_get_object(object, path, offset, false);
    if(obj == NULL) {
        retval = pcm_dm_add_inst(object, path, offset, &transaction);
        when_false_trace(retval, exit, ERROR, "Failed to add the instance [%s]", pcm_path_element_da_get(path, -1));
    } else {
        amxd_trans_select_object(&transaction, obj);
    }

    amxc_var_for_each(var, value) {
        uint32_t var_type = amxc_var_type_of(var);
        const char* key = amxc_var_key(var);
        if((var_type != AMXC_VAR_ID_HTABLE) && (var_type != AMXC_VAR_ID_LIST)) {
            /* Check for __VAR_SETS_MI_IND__ or __VAR_SETS_PK_IND__ during import,
               We can remove this check condition when we will no longer use the above indicators*/
            if(pcm_dm_is_instance_indicator(key)) {
                continue;
            }

            /* Check for upc_overwrite during import */
            if(pcm_dm_is_upc_overwrite(object, path, offset, key)) {
                continue;
            }

            amxd_trans_set_param(&transaction, key, var);
        }
    }

    status = amxd_trans_apply(&transaction, pcm_svc_dm());
    when_failed_trace(status, exit, ERROR, "Failed to set the parameters - using path '%s', error_code: %d", pcm_path_da_get(path), status);
    retval = true;

exit:
    amxd_trans_clean(&transaction);
    return retval;
}

bool pcm_dm_object_remove(amxd_object_t* object, const pcm_path_t* path, int offset, UNUSED pcm_dataset_t dataset) {
    amxd_object_t* obj = NULL;

    if((object == NULL) || (path == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument");
        return false;
    }

    obj = pcm_dm_get_object(object, path, offset, false);
    if(obj == NULL) {
        return true;
    }

    if(amxd_object_instance == amxd_object_get_type(obj)) {
        amxd_object_emit_del_inst(obj);
        amxd_object_delete(&obj);
        return true;
    } else if(amxd_object_template == amxd_object_get_type(obj)) {
        amxd_object_for_each(instance, it, obj) {
            amxd_object_t* inst = amxc_container_of(it, amxd_object_t, it);
            amxd_object_emit_del_inst(inst);
            amxd_object_delete(&inst);
        }
        return true;
    }

    SAH_TRACEZ_ERROR(ME, "Deletion of non-instance objects is not allowed");
    return false;
}

bool pcm_dm_for_each(amxd_object_t* object, bool usersetting, bool changed, dm_for_each_cb cb, void* userdata) {
    pcm_path_t path;
    char* obj_path = NULL;
    bool result;

    if((object == NULL) || !cb) {
        return false;
    }

    if(!amxd_object_is_attr_set(object, amxd_oattr_persistent)) {
        return true;
    }

    pcm_path_initialize(&path);
    obj_path = amxd_object_get_path(object, AMXD_OBJECT_NAMED);
    pcm_path_set(&path, obj_path);
    pcm_path_element_push_root(&path, "hgwconfig");
    free(obj_path);

    result = pcm_dm_for_each_object(object, usersetting, changed, &path, cb, userdata);
    pcm_path_cleanup(&path);

    return result;
}

/* Static Functions */
/* return true if parameter should be selected */
static bool pcm_dm_parameter_selected(const amxd_param_t* param, bool changed) {
    bool retval = false;
    pcm_ctx_t* ctx = NULL;
    ctx = pcm_ctx_get();
    when_null_trace(ctx, exit, ERROR, "Could not retrieve the CTX content");

    if(changed == amxd_param_has_flag(param, "upc_changed")) {
        amxc_llist_for_each(iter, &ctx->imex.userflags) {
            amxc_string_t* userflag = amxc_container_of(iter, amxc_string_t, it);
            if(amxc_string_get(userflag, 0)[0] == '!') {
                if(amxd_param_has_flag(param, amxc_string_get(userflag, 1))) {
                    retval = false;
                    break;
                }
            }
            if(amxd_param_has_flag(param, amxc_string_get(userflag, 0))) {
                retval = true;
            }
        }
    }

exit:
    return retval;
}

static bool pcm_dm_for_each_deleted_instance(bool usersetting, pcm_path_t* path, dm_for_each_cb cb, void* userdata) {
    bool ret = false;
    amxc_var_t del_instances_list;

    when_null(path, exit);

    /* find all deleted instance wit the same path and add it a new llist */

    amxc_var_init(&del_instances_list);
    amxc_var_set_type(&del_instances_list, AMXC_VAR_ID_LIST);
    pcm_inst_findpath(path, pcm_svc_deleted_instances(), &del_instances_list);

    amxc_var_for_each(instance, &del_instances_list) {
        amxc_var_t value;
        const char* ipath = GET_CHAR(instance, "path");
        const char* iname = GET_CHAR(instance, "name");
        const char* itype = GET_CHAR(instance, "type");
        const char* iprimkey = GET_CHAR(instance, "primkey");
        const char* iprimkeyvalue = GET_CHAR(instance, "primkeyvalue");

        when_str_empty(ipath, fail);
        when_str_empty(iname, fail);
        when_str_empty(itype, fail);

        /* in case of usersetting, skip only upc instances */
        if(usersetting && (0 != strcmp(itype, "usersetting"))) {
            continue;
        }

        pcm_path_element_push(path, iname);
        if(iprimkey && *iprimkey) {
            pcm_path_element_push(path, iprimkey);
        }

        amxc_var_init(&value);
        if(iprimkeyvalue && iprimkeyvalue) {
            amxc_var_set(cstring_t, &value, iprimkeyvalue);
        }

        if(!cb(path, &value, pcm_dataset_del, userdata)) {
            SAH_TRACEZ_ERROR(ME, "Callback function returned error");
            amxc_var_clean(&value);
            if(iprimkey && *iprimkey) {
                pcm_path_element_pop(path);
            }
            pcm_path_element_pop(path);
            break;
        }

        amxc_var_clean(&value);
        if(iprimkey && *iprimkey) {
            pcm_path_element_pop(path);
        }
        pcm_path_element_pop(path);
    }
    ret = true;

fail:
    amxc_var_clean(&del_instances_list);
exit:
    return ret;
}

static void pcm_cnv_to_search_path(const char* src_path_str, pcm_path_t* ret_path) {
    amxc_string_t full_path;
    amxc_string_t search_path;
    amxc_string_t root_obj;
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t data;
    const char* alias = NULL;

    amxc_string_init(&full_path, 0);
    amxc_string_init(&root_obj, 0);
    amxc_string_init(&search_path, 0);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_str_empty_trace(src_path_str, exit, ERROR, "The reference parameter path is empty");
    when_null_trace(ret_path, exit, ERROR, "The return path is not initialized");

    pcm_path_set(ret_path, src_path_str);

    amxc_string_setf(&root_obj, "%s.", pcm_path_element_da_get(ret_path, 0));
    ctx = amxb_be_who_has(amxc_string_get(&root_obj, 0));
    when_null_trace(ctx, exit, ERROR, "Could not find the bus for the root object %s", amxc_string_get(&root_obj, 0));

    amxc_string_setf(&full_path, "%s.", src_path_str);
    amxb_get(ctx, amxc_string_get(&full_path, 0), 0, &data, 5);

    alias = GET_CHAR(GETP_ARG(&data, "0.0."), "Alias");
    when_str_empty_trace(alias, exit, ERROR, "The path does not contain Alias parameter");

    amxc_string_setf(&search_path, "[Alias == \"%s\"]", alias);
    pcm_path_element_pop(ret_path);
    pcm_path_element_push(ret_path, amxc_string_get(&search_path, 0));

exit:
    amxc_var_clean(&data);
    amxc_string_clean(&search_path);
    amxc_string_clean(&root_obj);
    amxc_string_clean(&full_path);
    return;
}

static void pcm_param_get_value(amxd_param_t* param, amxc_var_t* ret_value) {
    pcm_path_t ret_path;

    pcm_path_initialize(&ret_path);
    when_null_trace(param, exit, ERROR, "The param is null");
    when_null_trace(ret_value, exit, ERROR, "The return value is not initialized");

    if(amxd_param_has_flag(param, "upc_ref")) {
        pcm_cnv_to_search_path(GET_CHAR(&param->value, 0), &ret_path);
        amxc_var_set(cstring_t, ret_value, pcm_path_da_get(&ret_path));
    } else {
        amxc_var_copy(ret_value, &param->value);
    }

exit:
    pcm_path_cleanup(&ret_path);
    return;
}

static bool pcm_dm_for_each_object(amxd_object_t* object, bool usersetting, bool changed, pcm_path_t* path, dm_for_each_cb cb, void* userdata) {
    bool ret = true;
    amxc_llist_it_t* lit = NULL;

    if(amxd_object_template == amxd_object_get_type(object)) {
        lit = amxd_object_first_instance(object);

        /*
         * Reverse logic:
         * Every deleted usersetting instance, is also a deleted upc instance (usersetting > upc).
         * Therefore, always check deleted usersetting instances, and only check deleted upc instances
         * when 'usersetting' is false.
         */
        if(!pcm_dm_for_each_deleted_instance(usersetting, path, cb, userdata)) {
            return false;
        }
    } else {
        lit = amxd_object_first_child(object);

        amxd_object_for_each(parameter, it, object) {
            amxd_param_t* param = amxc_container_of(it, amxd_param_t, it);
            amxc_var_t value;

            if(!pcm_dm_parameter_selected(param, changed)) {
                continue;
            }

            amxc_var_init(&value);
            pcm_path_element_push(path, amxd_param_get_name(param));
            pcm_param_get_value(param, &value);

            if(!cb(path, &value, pcm_dataset_set, userdata)) {
                SAH_TRACEZ_ERROR(ME, "Callback function returned error");
                pcm_path_element_pop(path);
                ret = false;
            }
            amxc_var_clean(&value);
            pcm_path_element_pop(path);
        }
    }

    while(lit) {
        amxc_llist_it_t* it = lit;
        amxd_object_t* child = amxc_container_of(it, amxd_object_t, it);
        if(amxd_object_is_attr_set(object, amxd_oattr_persistent)) {

            pcm_path_element_push(path, amxd_object_get_name(child, AMXD_OBJECT_NAMED));
            if(!pcm_dm_for_each_object(child, usersetting, changed, path, cb, userdata)) {
                ret = false;
            }

            pcm_path_element_pop(path);
        }

        lit = amxc_llist_it_get_next(lit);
    }

    return ret;
}

static bool pcm_dm_key_is_index(const char* key) {
    bool retval = false;

    when_str_empty_trace(key, exit, ERROR, "The key is empty");
    retval = (key[0] >= '0' && key[0] <= '9');
exit:
    return retval;
}

static bool pcm_dm_is_upc_overwrite(amxd_object_t* root_obj, const pcm_path_t* path, int offset, const char* param_key) {
    bool retval = false;
    amxd_param_t* param = NULL;
    amxd_object_t* obj = NULL;

    when_null_trace(root_obj, exit, ERROR, "Object is NULL");
    when_null_trace(path, exit, ERROR, "path is NULL");
    when_str_empty_trace(param_key, exit, ERROR, "Parameter key is not provided");

    obj = pcm_dm_get_object(root_obj, path, offset, false);
    param = amxd_object_get_param_def(obj, param_key);
    if(amxd_param_has_flag(param, "upc_overwrite")) {
        SAH_TRACEZ_INFO(ME, "Not selecting : UPC Overwrite flag detected - param:[%s]", amxd_param_get_name(param));
        retval = true;
    }

exit:
    return retval;
}

static bool pcm_dm_is_instance_indicator(const char* param_key) {
    bool retval = false;
    when_str_empty_trace(param_key, exit, ERROR, "Parameter is empty");

    if((0 == strcmp(param_key, "__VAR_SETS_MI_IND__")) ||
       (0 == strcmp(param_key, "__VAR_SETS_PK_IND__"))) {
        retval = true;
    }

exit:
    return retval;
}
