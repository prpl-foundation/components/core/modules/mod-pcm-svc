/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_pcm_svc.h"
#include "mod_pcm_imex.h"
#include "mod_pcm_ctx.h"
#include "mod_pcm_reg.h"

#define ME "mod_pcm_svc"
#define PCM_MANAGER  "PersistentConfiguration."

static amxb_bus_ctx_t* context = NULL;

static bool pcm_reg_get_registration_data(pcm_ctx_t* ctx, amxc_var_t* data);
static amxd_status_t pcm_perform_register(void);
static bool pcm_reg_restore_on_register(amxc_var_t* data);
static void pcm_reg_handle_register_response(amxc_var_t* ret);

amxb_bus_ctx_t* pcm_reg_get_pcm_manager_ctx(void) {
    if(NULL == context) {
        context = amxb_be_who_has(PCM_MANAGER);
    }
    return context;
}

amxd_status_t pcm_reg_register(void) {
    amxd_status_t status = amxd_status_unknown_error;
    pcm_ctx_t* ctx = pcm_ctx_get();
    when_null(ctx, exit);

    ctx->state = pcm_state_registering;

    status = pcm_perform_register();
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to register to PCM service");
        goto exit;
    }

    status = amxd_status_ok;
exit:
    return status;
}

amxd_status_t pcm_reg_perform_unregister(const char* object) {
    amxd_status_t rc = amxd_status_unknown_error;
    pcm_ctx_t* ctx = pcm_ctx_get();
    amxc_var_t args;
    amxc_string_t name;


    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_string_init(&name, 0);

    when_null(ctx, exit);

    amxc_string_setf(&name, "%s_%s", ctx->info.name, object);
    amxc_var_add_key(cstring_t, &args, "name", amxc_string_get(&name, 0));

    if(AMXB_STATUS_OK == amxb_call(pcm_reg_get_pcm_manager_ctx(), PCM_MANAGER, "unregisterSvc", &args, NULL, 5)) {
        rc = amxd_status_ok;
    }

exit:
    amxc_string_clean(&name);
    amxc_var_clean(&args);
    return rc;
}

/*Static Functions*/
static bool pcm_reg_get_registration_data(pcm_ctx_t* ctx, amxc_var_t* data) {
    bool rv = false;
    amxc_var_t* schemes = NULL;
    amxc_var_t* schemeTable = NULL;
    amxc_var_t* info = NULL;
    char version[10] = "";

    when_null_trace(ctx, exit, ERROR, "Invalid argument");
    when_null_trace(data, exit, ERROR, "Invalid argument");

    pcm_version_get(version, sizeof(version));

    info = amxc_var_add_key(amxc_htable_t, data, "info", NULL);

    amxc_var_add_key(cstring_t, info, "name", ctx->info.name);
    amxc_var_add_key(cstring_t, info, "version", version);
    amxc_var_add_key(cstring_t, info, "dmRoot", ctx->object->name);
    amxc_var_add_key(cstring_t, info, "rpcImport", ctx->info.rpcImport);
    amxc_var_add_key(cstring_t, info, "rpcExport", ctx->info.rpcExport);
    amxc_var_add_key(cstring_t, info, "backup_file", ctx->info.file);
    amxc_var_add_key(uint32_t, info, "priority", ctx->info.priority);
    amxc_var_add_key(bool, info, "outOfOrder", true);

    /* add scheme with the same name as the dmRoot - as Pcm uses the schemeName to save */
    schemes = amxc_var_add_key(amxc_llist_t, data, "schemes", NULL);
    schemeTable = amxc_var_add(amxc_htable_t, schemes, NULL);
    amxc_var_add_key(cstring_t, schemeTable, "id", ctx->object->name);

    rv = true;
exit:
    return rv;
}

static amxd_status_t pcm_perform_register(void) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* data = NULL;
    amxc_string_t name;
    pcm_ctx_t* ctx = pcm_ctx_get();

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&name, 0);
    amxc_string_setf(&name, "%s_%s", ctx->info.name, ctx->object->name);

    when_null(ctx, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", amxc_string_get(&name, 0));

    data = amxc_var_add_key(amxc_htable_t, &args, "data", NULL);
    pcm_reg_get_registration_data(ctx, data);

    if(AMXB_STATUS_OK != amxb_call(pcm_reg_get_pcm_manager_ctx(), PCM_MANAGER, "registerSvc", &args, &ret, 5)) {
        SAH_TRACEZ_ERROR(ME, "%sregisterSvc() call Failed", PCM_MANAGER);
        goto exit;
    }
    pcm_reg_handle_register_response(&ret);

    rc = amxd_status_ok;
exit:
    amxc_string_clean(&name);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    return rc;
}

static bool pcm_reg_restore_on_register(amxc_var_t* data) {
    bool rv = false;
    pcm_ctx_t* ctx = NULL;
    amxc_string_t object_path;
    amxc_var_t args;
    amxc_var_t ret;
    amxd_status_t status = amxd_status_unknown_error;

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_string_init(&object_path, 0);

    ctx = pcm_ctx_get();
    when_null(ctx, exit);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_htable_t, &args, "data", amxc_var_constcast(amxc_htable_t, data));

    amxc_string_setf(&object_path, "%s.", amxd_object_get_name(ctx->object, AMXD_OBJECT_NAMED));

    status = amxb_call(pcm_reg_get_pcm_manager_ctx(), amxc_string_get(&object_path, 0), ctx->info.rpcImport, &args, &ret, 5);
    if(status != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to call the %s of the %s", ctx->info.rpcImport, ctx->info.name);
    }

    rv = true;
exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&args);
    amxc_string_clean(&object_path);
    return rv;
}

static void pcm_reg_handle_register_response(amxc_var_t* ret) {
    pcm_ctx_t* ctx = pcm_ctx_get();
    pcm_upc_t* backup_data = NULL;
    amxc_var_t args;
    amxc_string_t object_path;

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "ActionStatus", "ImportStatus");

    amxc_string_init(&object_path, 0);

    when_null(ctx, exit);
    when_null(ret, exit);

    if(ctx->state == pcm_state_registering) {
        SAH_TRACEZ_INFO(ME, "Plug-in '%s' registered successfully to the PCM service", ctx->info.name);
        ctx->state = pcm_state_ready;

        backup_data = (pcm_upc_t*) GETP_ARG(ret, "0.data");
        when_null(backup_data, exit);

        amxc_string_setf(&object_path, "%sService.%s_%s.", PCM_MANAGER, ctx->info.name, ctx->object->name);

        if(!pcm_reg_restore_on_register(backup_data)) {
            amxc_var_add_key(cstring_t, &args, "State", "Error");
            amxb_call(pcm_reg_get_pcm_manager_ctx(), amxc_string_get(&object_path, 0), "SetActionStatus", &args, NULL, 5);
            SAH_TRACEZ_ERROR(ME, "Failed to import initial UPC data");
            ctx->state = pcm_state_error;
            goto exit;
        }

        amxc_var_add_key(cstring_t, &args, "State", "Success");
        if(AMXB_STATUS_OK != amxb_call(pcm_reg_get_pcm_manager_ctx(), amxc_string_get(&object_path, 0), "SetActionStatus", &args, NULL, 5)) {
            SAH_TRACEZ_ERROR(ME, "Failed to set the ImportStatus of the %s", ctx->info.name);
        }

        SAH_TRACEZ_INFO(ME, "Initial UPC data imported");

    }
exit:
    amxc_var_clean(&args);
    amxc_string_clean(&object_path);
    return;
}
