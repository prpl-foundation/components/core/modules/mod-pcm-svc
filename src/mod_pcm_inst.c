/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_pcm_path.h"
#include "mod_pcm_inst.h"

#define ME "mod_pcm_svc"

/* create a new htable with some key-value pairs and add it the varlist (instances) */
void pcm_inst_add(amxc_var_t* instances, const char* path, const char* name,
                  const char* type, const char* primkey, const char* primkeyvalue) {
    amxc_var_t* inst = NULL;

    when_null(instances, exit);
    when_null(path, exit);
    when_null(name, exit);
    when_null(type, exit);

    inst = amxc_var_add(amxc_htable_t, instances, NULL);
    amxc_var_add_key(cstring_t, inst, "path", path);
    amxc_var_add_key(cstring_t, inst, "name", name);
    amxc_var_add_key(cstring_t, inst, "type", type);
    if(primkey) {
        amxc_var_add_key(cstring_t, inst, "primkey", primkey);
    }
    if(primkeyvalue) {
        amxc_var_add_key(cstring_t, inst, "primkeyvalue", primkeyvalue);
    }
exit:
    return;
}

void pcm_inst_remove(const amxc_var_t* instances,
                     const char* path,
                     const char* name) {

    when_null(path, exit);
    when_null(name, exit);

    amxc_var_for_each(inst, instances) {
        pcm_path_t ipcmpath;
        pcm_path_t pcm_path;
        const char* ipath = GET_CHAR(inst, "path");
        const char* iname = GET_CHAR(inst, "name");

        if(ipath == NULL) {
            continue;
        }

        if(iname == NULL) {
            continue;
        }

        pcm_path_initialize(&ipcmpath);
        pcm_path_set(&ipcmpath, ipath);
        pcm_path_element_push(&ipcmpath, iname);
        pcm_path_initialize(&pcm_path);
        pcm_path_set(&pcm_path, path);
        pcm_path_element_push(&pcm_path, name);

        SAH_TRACEZ_INFO(ME, "comparing path [%s] vs [%s]", pcm_path_da_get(&pcm_path), pcm_path_da_get(&ipcmpath));
        if(0 == pcm_path_compare(&pcm_path, &ipcmpath)) {
            SAH_TRACEZ_INFO(ME, "match found -> deleting");
            amxc_var_delete(&inst);
        }

        pcm_path_cleanup(&ipcmpath);
        pcm_path_cleanup(&pcm_path);
    }

exit:
    return;
}

void pcm_inst_findpath(const pcm_path_t* findpath,
                       const amxc_var_t* src_instances,
                       amxc_var_t* dst_instances) {

    amxc_var_for_each(src_inst, src_instances) {
        const char* path = GET_CHAR(src_inst, "path");
        const char* name = GET_CHAR(src_inst, "name");
        const char* type = GET_CHAR(src_inst, "type");
        const char* primkey = GET_CHAR(src_inst, "primkey");
        const char* primkeyvalue = GET_CHAR(src_inst, "primkeyvalue");

        pcm_path_t pcm_path;
        pcm_path_initialize(&pcm_path);
        pcm_path_set(&pcm_path, path);
        pcm_path_element_push_root(&pcm_path, "hgwconfig");

        if(0 == pcm_path_compare(&pcm_path, findpath)) {
            pcm_inst_add(dst_instances,
                         path,
                         name,
                         type,
                         primkey,
                         primkeyvalue);
        }

        pcm_path_cleanup(&pcm_path);
    }
}

bool pcm_svc_is_run_time(const char* path,
                         const char* name,
                         amxc_var_t* src_instances) {
    bool ret_val = false;
    pcm_path_t find_path;

    pcm_path_initialize(&find_path);
    when_str_empty_trace(path, exit, ERROR, "The instance path is empty");
    when_str_empty_trace(name, exit, ERROR, "The instance name is empty");

    pcm_path_set(&find_path, path);
    pcm_path_element_push(&find_path, name);

    amxc_var_for_each(src_inst, src_instances) {
        pcm_path_t pcm_path;
        const char* src_path = GET_CHAR(src_inst, "path");
        const char* src_name = GET_CHAR(src_inst, "name");

        pcm_path_initialize(&pcm_path);
        pcm_path_set(&pcm_path, src_path);
        pcm_path_element_push(&pcm_path, src_name);

        if(0 == pcm_path_compare(&pcm_path, &find_path)) {
            ret_val = true;
            pcm_path_cleanup(&pcm_path);
            goto exit;
        }
        pcm_path_cleanup(&pcm_path);
    }

exit:
    pcm_path_cleanup(&find_path);
    return ret_val;
}

int pcm_inst_write(const char* file, const amxc_var_t* instances) {
    int ret = 0;
    int fd = -1;
    int write_length = 0;
    variant_json_t* writer = NULL;

    when_str_empty_trace(file, exit, ERROR, "empty file");

    unlink(file);
    pcm_mk_path(file, 0777);

    fd = open(file, O_RDWR | O_CREAT, 0644);
    when_true_trace(fd == -1, exit, ERROR, "Failed to create file '%s' - (%s)", file, strerror(errno));

    amxj_writer_new(&writer, instances);
    write_length = amxj_write(writer, fd);

    amxj_writer_delete(&writer);
    close(fd);

    ret = write_length;

exit:
    return ret;
}

void pcm_inst_read(const char* file, amxc_var_t* const instances) {
    int fd = -1;
    int read_length = 0;
    amxc_var_t* var = NULL;
    variant_json_t* reader = NULL;

    when_str_empty_trace(file, exit, ERROR, "empty file");

    fd = open(file, O_RDONLY);
    when_true_trace(fd == -1, exit, WARNING, "File '%s' is not available", file);

    amxj_reader_new(&reader);
    read_length = amxj_read(reader, fd);
    while(read_length > 0) {
        read_length = amxj_read(reader, fd);
    }

    var = amxj_reader_result(reader);
    amxc_var_copy(instances, var);
    amxc_var_delete(&var);

    amxj_reader_delete(&reader);
    close(fd);

exit:
    return;
}
