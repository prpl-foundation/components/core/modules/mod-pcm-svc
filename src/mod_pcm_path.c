/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_pcm_path.h"

#define ME "mod_pcm_svc"
#define STR_EMPTY(x) (x) == NULL || (x)[0] == '\0'

pcm_path_t* pcm_path_create(void) {
    pcm_path_t* path = NULL;

    path = calloc(1, sizeof(pcm_path_t));
    when_null_trace(path, exit, ERROR, "Out of memory");

    pcm_path_initialize(path);

exit:
    return path;
}

void pcm_path_destroy(pcm_path_t* path) {
    pcm_path_cleanup(path);
    free(path);
}


bool pcm_path_initialize(pcm_path_t* path) {
    bool ret = false;

    when_null_trace(path, exit, ERROR, "Invalid argument");

    path->dirty = false;
    amxc_string_init(&(path->full), 50);
    amxc_llist_init(&(path->expl));
    path->num_elem = 0;
    ret = true;

exit:
    return ret;
}

void pcm_path_cleanup(pcm_path_t* path) {
    if(path) {
        path->dirty = false;
        amxc_string_clean(&(path->full));
        amxc_llist_clean(&(path->expl), amxc_string_list_it_free);
        path->num_elem = 0;

    }
}

static bool pcm_path_split(pcm_path_t* path, const char* new_path) {
    /*
     * Elements are split using a single dot '.' character.
     *
     * However, this character can be inside the value of a "named wildcard with value".
     * Such value will be enclosed by quote '"' characters.
     * Inside the value, the quote '"' character itself can be escaped with the backslash '\' character,
     * which is also used to escape itself.
     */
    bool retval = false;
    bool esc = false;
    char* path_copy = NULL;
    char* curr = NULL;
    char* sep = NULL;
    char* quote = NULL;

    path_copy = strdup(new_path);
    when_null(path_copy, error);

    for(curr = path_copy; *curr != 0;) {
        sep = strchr(curr, PATH_SEP);
        quote = strchr(curr, PATH_QUOTE);

        /* Quote before separator? Find closing quote */
        if((sep != NULL) && (quote != NULL) && (quote < sep)) {
            for(esc = false, quote++; *quote != 0; quote++) {
                if((*quote == PATH_QUOTE) && !esc) {
                    break;
                }

                esc = (*quote == PATH_ESCAPE && !esc);
            }

            sep = strchr(quote, PATH_SEP);
        }

        if(sep != NULL) {
            *sep = 0;
        }

        amxc_llist_add_string(&(path->expl), curr);
        path->num_elem++;

        if(sep == NULL) {
            break;
        }

        curr = sep + 1;
    }

    path->dirty = (path->num_elem > 0);

    retval = true;

error:
    free(path_copy);

    if(!retval) {
        pcm_path_cleanup(path);
    }

    return retval;
}


bool pcm_path_set(pcm_path_t* path, const char* new_path) {
    bool ret = false;

    when_null_trace(path, exit, ERROR, "Invalid argument");

    pcm_path_cleanup(path);

    if(STR_EMPTY(new_path)) {
        return true;
    }

    ret = pcm_path_split(path, new_path);

exit:
    return ret;
}

bool pcm_path_element_push(pcm_path_t* path, const char* element) {
    bool ret = false;

    when_null_trace(path, exit, ERROR, "Invalid argument");

    if(STR_EMPTY(element)) {
        return true;
    }

    amxc_llist_add_string(&(path->expl), element);
    path->num_elem++;
    path->dirty = true;
    ret = true;

exit:
    return ret;
}

bool pcm_path_element_push_root(pcm_path_t* path, const char* element) {
    bool ret = false;
    amxc_llist_it_t* it = NULL;
    amxc_string_t* string = NULL;

    when_null(path, exit);
    when_str_empty(element, exit);

    /* prepend string */
    when_failed(amxc_string_new(&string, 0), exit);
    when_failed(amxc_string_append(string, element, strlen(element)), exit);
    amxc_llist_prepend(&path->expl, &string->it);
    it = &string->it;

    path->num_elem++;
    path->dirty = true;

    ret = true;
exit:
    if(it == NULL) {
        amxc_string_delete(&string);
    }
    return ret;
}

bool pcm_path_element_pop(pcm_path_t* path) {
    bool ret = false;

    when_null_trace(path, exit, ERROR, "Invalid argument");

    amxc_llist_it_t* it = amxc_llist_get_last(&(path->expl));
    amxc_llist_it_take(it);
    amxc_string_list_it_free(it);

    path->num_elem--;
    path->dirty = true;
    ret = true;

exit:
    return ret;
}

bool pcm_path_element_pop_root(pcm_path_t* path) {
    bool ret = false;

    when_null_trace(path, exit, ERROR, "Invalid argument");

    amxc_llist_it_t* it = amxc_llist_get_first(&(path->expl));
    amxc_llist_it_take(it);
    amxc_string_list_it_free(it);

    path->num_elem--;
    path->dirty = true;
    ret = true;

exit:
    return ret;
}

int pcm_path_element_count(const pcm_path_t* path) {
    int ret = -1;

    when_null_trace(path, exit, ERROR, "Invalid argument");
    ret = path->num_elem;

exit:
    return ret;
}

static bool pcm_path_realIndex(const pcm_path_t* a, const pcm_path_t* b, int* start_at, int* end_at) {
    int count = 0;

    if(a == NULL) {
        return false;
    }

    count = a->num_elem;

    if(start_at != NULL) {
        if(*start_at < 0) {
            *start_at += count;
        }

        if((*start_at < 0) || (*start_at >= count) || (b && (*start_at >= b->num_elem))) {
            return false;
        }
    }

    if(end_at != NULL) {
        if(*end_at < 0) {
            *end_at += count;
        }

        if((*end_at < 0) || (*end_at >= count) || (b && (*end_at >= b->num_elem))) {
            return false;
        }
    }

    if((start_at != NULL) && (end_at != NULL) && (*end_at < *start_at)) {
        return false;
    }

    return true;
}

const char* pcm_path_element_da_get(const pcm_path_t* path, int at) {
    if(path == NULL) {
        return NULL;
    }

    if(!pcm_path_realIndex(path, NULL, &at, NULL)) {
        return NULL;
    }

    return amxc_string_get_text_from_llist(&(path->expl), (unsigned int) at);
}

const char* pcm_path_da_get(const pcm_path_t* path) {
    pcm_path_t* pathX = NULL;

    if(path == NULL) {
        return NULL;
    }


    if(path->dirty) {
        /*
         * Because we potentially alter the contents of 'path',
         * we need to cast it to a non-const.
         */
        pathX = (pcm_path_t*) path;

        amxc_string_clean(&pathX->full);
        amxc_string_join_llist(&(pathX->full), &(path->expl), PATH_SEP);
        pathX->dirty = false;
    }

    return path->full.buffer;
}

bool pcm_path_copy(pcm_path_t* dst, const pcm_path_t* src) {
    if((dst == NULL) || (src == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument");
        return false;
    }

    return pcm_path_set(dst, pcm_path_da_get(src));
}

int pcm_path_compare(const pcm_path_t* a, const pcm_path_t* b) {
    if((a == NULL) || (b == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument");
        return INT32_MAX;
    }

    if(a->num_elem != b->num_elem) {
        return a->num_elem - b->num_elem;
    }

    return pcm_path_comparePartial(a, b, 0, -1);
}

int pcm_path_comparePartial(const pcm_path_t* a, const pcm_path_t* b, int start_at, int end_at) {
    int res = 0;
    const amxc_string_t* str_a = NULL;
    const amxc_string_t* str_b = NULL;
    amxc_llist_it_t* iter_a = NULL;
    amxc_llist_it_t* iter_b = NULL;

    if((a == NULL) || (b == NULL)) {
        SAH_TRACEZ_ERROR(ME, "Invalid argument");
        return INT32_MAX;
    }

    if(!pcm_path_realIndex(a, b, &start_at, &end_at)) {
        SAH_TRACEZ_ERROR(ME, "Index out of bounds");
        return INT32_MAX;
    }

    iter_a = amxc_llist_get_at(&(a->expl), (unsigned int) start_at);
    iter_b = amxc_llist_get_at(&(b->expl), (unsigned int) start_at);

    for(; start_at <= end_at; start_at++) {
        if((iter_a == NULL) || (iter_b == NULL)) {
            SAH_TRACEZ_ERROR(ME, "Failed to get element");
            return INT32_MAX;
        }

        str_a = amxc_container_of(iter_a, amxc_string_t, it);
        when_null(str_a, exit);
        str_b = amxc_container_of(iter_b, amxc_string_t, it);
        when_null(str_b, exit);

        res = strcmp(str_a->buffer, str_b->buffer);
        if(res != 0) {
            return res;
        }

        iter_a = amxc_llist_it_get_next(iter_a);
        iter_b = amxc_llist_it_get_next(iter_b);
    }

exit:
    return 0;
}
