/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <string.h>
#include <stdlib.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc_macros.h>

#include "mod_pcm_upc.h"
#include "mod_pcm_path.h"
#include "mod_pcm_svc.h"
#include "mod_pcm_ctx.h"

#define ME "mod_pcm_svc"
#define STR_EMPTY(x) (x) == NULL || (x)[0] == '\0'

static const char* upc_dataset_names[pcm_dataset_max] = { "set", "add", "delete" };
static const char* upc_version_name = "version";
static const char* upc_userflags = "userflags";
static const char* upc_type_name = "type";
static const char* upc_type_upc = "upc";
static const char* upc_type_usersetting = "usersetting";

static amxc_var_t* pcm_upc_get_element(amxc_var_t* curr, const pcm_path_t* path, bool withLeaf, bool create);
static amxc_var_t* pcm_upc_get_dataset(const pcm_upc_t* data, pcm_dataset_t dataset);
static bool pcm_upc_for_each_element(amxc_var_t* curr, pcm_path_t* path, pcm_dataset_t dataset, upc_forEach_cb cb, void* userdata);


pcm_upc_t* pcm_upc_create(bool usersetting, amxc_string_t* userflags) {
    pcm_upc_t* data = NULL;

    data = calloc(1, sizeof(pcm_upc_t));
    when_null(data, exit);

    pcm_upc_initialize(data, usersetting, userflags);

exit:
    return data;
}

void pcm_upc_destroy(pcm_upc_t* data) {
    if(data) {
        pcm_upc_cleanup(data);

        free(data);
    }
}

bool pcm_upc_initialize(pcm_upc_t* data, bool usersetting, amxc_string_t* userflags) {
    bool ret = false;
    char version[10] = "";
    pcm_ctx_t* ctx = NULL;

    ctx = pcm_ctx_get();
    when_null_trace(ctx, exit, ERROR, "Could not retrieve the CTX content");
    when_null_trace(data, exit, ERROR, "Invalid argument");

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    pcm_version_get(version, sizeof(version));
    amxc_var_add_key(cstring_t, data, upc_version_name, version);
    amxc_var_add_key(cstring_t, data, upc_type_name, usersetting ? upc_type_usersetting : upc_type_upc);
    amxc_var_add_key(amxc_htable_t, data, upc_dataset_names[pcm_dataset_set], NULL);
    amxc_var_add_key(amxc_htable_t, data, upc_dataset_names[pcm_dataset_del], NULL);
    amxc_var_add_key(amxc_htable_t, data, upc_dataset_names[pcm_dataset_add], NULL);
    amxc_var_add_key(cstring_t, data, upc_userflags, amxc_string_get(userflags, 0));
    ret = true;

exit:
    return ret;
}

void pcm_upc_cleanup(pcm_upc_t* data) {
    if(data) {
        amxc_var_clean((amxc_var_t*) data);
    }
}

bool pcm_upc_is_valid(const pcm_upc_t* data) {
    bool rv = false;
    const char* type = NULL;
    const char* version = NULL;
    unsigned int v_major = 0;
    unsigned int v_minor = 0;

    when_null_trace(data, exit, ERROR, "UPC data is NULL");

    version = GET_CHAR(data, upc_version_name);

    when_false_trace(pcm_version_parse(version, &v_major, &v_minor),
                     exit, ERROR, "Could not parse the UPC version");
    when_true_trace(pcm_version_verify(v_major, v_minor) == pcm_version_unsupported,
                    exit, ERROR, "UPC data version is unsupported");


    type = GET_CHAR(data, upc_type_name);
    when_str_empty_trace(type, exit, ERROR, "UPC data type is empty");
    when_true_trace(strcmp(type, upc_type_upc) && strcmp(type, upc_type_usersetting),
                    exit, ERROR, "Invalid UPC type");

    when_null_trace(pcm_upc_get_dataset(data, pcm_dataset_set), exit, ERROR, "Could not get the \"set\" dataset");
    when_null_trace(pcm_upc_get_dataset(data, pcm_dataset_del), exit, ERROR, "Could not get the \"delete\" dataset");
    when_null_trace(pcm_upc_get_dataset(data, pcm_dataset_add), exit, ERROR, "Could not get the \"add\" dataset");

    SAH_TRACEZ_INFO(ME, "The UPC data is valid");
    rv = true;

exit:
    return rv;
}

bool pcm_upc_set(amxc_var_t* data, const pcm_path_t* path, amxc_var_t* value, pcm_dataset_t dataset) {
    amxc_var_t* leaf = NULL;
    amxc_var_t* elem = NULL;
    const amxc_htable_t* htable = NULL;
    amxc_htable_it_t* hit = NULL;

    if((data == NULL) || (path == NULL) || (value == NULL)) {
        return false;
    }

    switch(amxc_var_type_of(value)) {
    case AMXC_VAR_ID_NULL:         //0
    case AMXC_VAR_ID_CSTRING:      //1
    case AMXC_VAR_ID_INT8:         //2
    case AMXC_VAR_ID_INT16:        //3
    case AMXC_VAR_ID_INT32:        //4
    case AMXC_VAR_ID_INT64:        //5
    case AMXC_VAR_ID_UINT8:        //6
    case AMXC_VAR_ID_UINT16:       //7
    case AMXC_VAR_ID_UINT32:       //8
    case AMXC_VAR_ID_UINT64:       //9
    case AMXC_VAR_ID_FLOAT:        //10
    case AMXC_VAR_ID_DOUBLE:       //11
    case AMXC_VAR_ID_BOOL:         //12
    case AMXC_VAR_ID_TIMESTAMP:    //16
    case AMXC_VAR_ID_CSV_STRING:   //17
    case AMXC_VAR_ID_SSV_STRING:   //18
        break;
    default:
        return false;
        break;
    }

    elem = pcm_upc_get_element(pcm_upc_get_dataset(data, dataset), path, true, true);
    if(elem == NULL) {
        return false;
    }

    htable = amxc_var_constcast(amxc_htable_t, elem);
    hit = amxc_htable_get(htable, pcm_path_element_da_get(path, -1));
    if(hit != NULL) {
        /* existing leaf - update value */
        leaf = amxc_var_from_htable_it(hit);
        if(amxc_var_type_of(leaf) == AMXC_VAR_ID_HTABLE) {
            return false;
        }

        if(0 != amxc_var_copy(leaf, value)) {
            return false;
        }
    } else {
        /* new leaf - set new key with value */
        leaf = amxc_var_add_key(amxc_htable_t, elem, pcm_path_element_da_get(path, -1), NULL);
        if(leaf == NULL) {
            return false;
        }
        if(0 != amxc_var_copy(leaf, value)) {
            return false;
        }
    }

    return true;
}

bool pcm_upc_for_each(const pcm_upc_t* data, upc_forEach_cb cb, void* userdata) {
    bool retval = false;
    const char* root_obj = NULL;
    pcm_path_t path;
    pcm_dataset_t i = 0;
    pcm_ctx_t* ctx = (pcm_ctx_t*) userdata;

    pcm_path_initialize(&path);

    when_null_trace(data, exit, ERROR, "UPC data is NULL");
    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    when_null_trace(cb, exit, ERROR, "The callback function is not provided");

    root_obj = amxd_object_get_name(ctx->object, AMXD_OBJECT_NAMED);
    when_str_empty_trace(root_obj, exit, ERROR, "The ctx does not contain the Root Object");
    pcm_path_element_push(&path, root_obj);

    for(i = 0; i < pcm_dataset_max; i++) {
        amxc_var_t* dataset_var = NULL;

#ifndef MOD_PCM_SVC_UNIT_TEST
        // Ignore the backup file's delete section data set for now.
        // We need to further investigate the delete operation so that it does not damage the DataModel
        if(i == pcm_dataset_del) {
            continue;
        }
#endif

        dataset_var = pcm_upc_get_dataset(data, i);
        when_null_trace(dataset_var, exit, ERROR, "Could not get the dataset [%s]", upc_dataset_names[i]);

        /* skip the empty datasets*/
        if(amxc_var_get_first(dataset_var) == NULL) {
            SAH_TRACEZ_INFO(ME, "The dataset [%s] is empty", upc_dataset_names[i]);
            retval = true;
            continue;
        }

        /* skip hgwconfig element */
        dataset_var = GET_ARG(dataset_var, "hgwconfig");
        when_null_trace(dataset_var, exit, ERROR,
                        "The dataset [%s] does not contain the hgwconfig", upc_dataset_names[i]);

        dataset_var = GET_ARG(dataset_var, root_obj);
        when_null_trace(root_obj, exit, ERROR, "There is not root object in the dataset [%s]", upc_dataset_names[i]);

        SAH_TRACEZ_INFO(ME, "Start importing all the elements of the dataset[%s]", upc_dataset_names[i]);
        retval = pcm_upc_for_each_element(dataset_var, &path, i, cb, userdata);
        when_false_trace(retval, exit, ERROR, "Error while looping over UPC element of dataset [%s]", upc_dataset_names[i]);
    }
    SAH_TRACEZ_INFO(ME, "All the datasets have been imported correctly");

exit:
    pcm_path_cleanup(&path);
    return retval;
}

amxc_var_t* pcm_add_element(amxc_var_t* curr, uint16_t type_id, const char* data) {
    amxc_var_t* elem = NULL;
    if(type_id == AMXC_VAR_ID_LIST) {
        elem = amxc_var_add_key(amxc_llist_t, curr, data, NULL);
        elem = amxc_var_add(amxc_htable_t, elem, NULL);
    } else if((type_id == AMXC_VAR_ID_HTABLE) && (amxc_var_type_of(curr) == AMXC_VAR_ID_HTABLE)) {
        elem = amxc_var_add_key(amxc_htable_t, curr, data, NULL);
    } else if((type_id == AMXC_VAR_ID_HTABLE) && (amxc_var_type_of(curr) == AMXC_VAR_ID_LIST)) {
        elem = amxc_var_add(amxc_htable_t, curr, NULL);
        elem = amxc_var_add_key(amxc_htable_t, elem, data, NULL);
    }
    return elem;
}

amxc_var_t* pcm_get_element(amxc_var_t* curr, const char* elem_name) {
    amxc_var_t* elem = NULL;
    if(amxc_var_type_of(curr) == AMXC_VAR_ID_LIST) {
        amxc_var_for_each(inst, curr) {
            elem = amxc_var_get_key(inst, elem_name, AMXC_VAR_FLAG_DEFAULT);
            if(elem != NULL) {
                break;
            }
        }
    } else if(amxc_var_type_of(curr) == AMXC_VAR_ID_HTABLE) {
        elem = amxc_var_get_path(curr, elem_name, AMXC_VAR_FLAG_DEFAULT);
    }
    return elem;
}

/* Static Functions */
static amxc_var_t* pcm_upc_get_element(amxc_var_t* curr, const pcm_path_t* path, bool withLeaf, bool create) {
    int i = 0;
    int count = 0;
    pcm_path_t object_path;
    amxc_var_t* ret = NULL;

    pcm_path_initialize(&object_path);
    when_null_trace(curr, exit, ERROR, "Invalid argument");
    when_null_trace(path, exit, ERROR, "Invalid argument");

    count = pcm_path_element_count(path);
    when_true_status(count == 0, exit, ret = curr);

    /* Path contains the leaf element, ignore it */
    if(withLeaf) {
        count--;
    }

    for(i = 0; i < count; i++) {
        amxd_object_t* object = NULL;
        amxc_var_t* elem = NULL;
        const char* elem_name = NULL;
        uint16_t type_id = 0;
        elem_name = pcm_path_element_da_get(path, i);
        when_true_trace(STR_EMPTY(elem_name), exit, ERROR, "The %dst element in path is empty", i);

        if(strcmp(elem_name, "hgwconfig") != 0) {
            pcm_path_element_push(&object_path, elem_name);
            object = amxd_dm_findf(pcm_svc_dm(), "%s", pcm_path_da_get(&object_path));
        }
        type_id = (object && (amxd_object_template == amxd_object_get_type(object)))? AMXC_VAR_ID_LIST: AMXC_VAR_ID_HTABLE;
        elem = pcm_get_element(curr, elem_name);
        if(elem == NULL) {
            when_false(create, exit);
            elem = pcm_add_element(curr, type_id, elem_name);
        } else {
            when_false(((amxc_var_type_of(elem) == AMXC_VAR_ID_LIST) ||
                        (amxc_var_type_of(elem) == AMXC_VAR_ID_HTABLE)), exit);
        }
        curr = elem;
    }
    ret = curr;

exit:
    pcm_path_cleanup(&object_path);
    return ret;
}

static amxc_var_t* pcm_upc_get_dataset(const pcm_upc_t* data, pcm_dataset_t dataset) {
    const amxc_htable_t* htable = amxc_var_constcast(amxc_htable_t, data);
    amxc_htable_it_t* hit = amxc_htable_get(htable, upc_dataset_names[dataset]);

    if(hit == NULL) {
        return NULL;
    }
    return amxc_var_from_htable_it(hit);
}

static bool pcm_upc_for_each_element(amxc_var_t* curr, pcm_path_t* path, pcm_dataset_t dataset, upc_forEach_cb cb, void* userdata) {
    const char* key = NULL;
    uint32_t var_type = AMXC_VAR_ID_NULL;
    bool ret_val = false;

    when_null_trace(path, exit, ERROR, "Invalid argument: path is NULL");
    when_null_trace(curr, exit, ERROR, "Invalid argument: variant is not provided for the path %s", pcm_path_da_get(path));

    ret_val = cb(path, curr, dataset, userdata);
    when_false_trace(ret_val, exit, ERROR, "Failed to set the parameter - using path '%s'\n", pcm_path_da_get(path));

    amxc_var_for_each(var, curr) {
        var = (amxc_var_type_of(curr) == AMXC_VAR_ID_LIST)? GETI_ARG(var, 0):var;
        var_type = amxc_var_type_of(var);
        key = amxc_var_key(var);

        pcm_path_element_push(path, key);

        if((var_type == AMXC_VAR_ID_HTABLE) || (var_type == AMXC_VAR_ID_LIST)) {
            ret_val = pcm_upc_for_each_element(var, path, dataset, cb, userdata);
            when_false_trace(ret_val, exit, ERROR, "The %s failed for the %s", pcm_svc_state(userdata) == pcm_state_importing ? "import" : "export",
                             pcm_path_da_get(path));
        }
        if(var_type != AMXC_VAR_ID_LIST) {
            pcm_path_element_pop(path);
        }
    }
    if(amxc_var_type_of(curr) == AMXC_VAR_ID_LIST) {
        pcm_path_element_pop(path);
    }
    ret_val = true;
exit:
    return ret_val;
}