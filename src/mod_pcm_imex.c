/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <string.h>
#include <stdlib.h>
#include <amxo/amxo.h>

#include "mod_pcm_imex.h"
#include "mod_pcm_def.h"
#include "mod_pcm_upc.h"
#include "mod_pcm_dm.h"
#include "mod_pcm_svc.h"
#include "mod_pcm_ctx.h"

#define ME "mod_pcm_svc"

static bool pcm_imex_finish(pcm_ctx_t* ctx);
static bool pcm_imex_error(pcm_ctx_t* ctx);
static bool pcm_imex_perform_remove(pcm_ctx_t* ctx, const pcm_path_t* path, amxc_var_t* value);
static bool pcm_imex_for_each_cb(const pcm_path_t* path, amxc_var_t* value,
                                 pcm_dataset_t dataset, void* userdata);
static pcm_upc_t* pcm_imex_start_export(pcm_ctx_t* ctx, bool usersetting, amxc_string_t* userflags);
static bool pcm_imex_start_import(pcm_ctx_t* ctx, const pcm_upc_t* data);
static bool pcm_imex_init_userflags(pcm_ctx_t* ctx, amxc_string_t* userflags);
static bool pcm_imex_is_template(pcm_ctx_t* ctx, const pcm_path_t* path);


bool pcm_imex_remove(pcm_ctx_t* ctx, const pcm_path_t* path) {
    bool retval = false;

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    when_null_trace(path, exit, ERROR, "The path is NULL");

    switch(pcm_svc_state(ctx)) {
    case pcm_state_exporting:
    case pcm_state_importing:
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Pcm context is not exporting/importing");
        goto exit;
        break;
    }
    retval = pcm_dm_object_remove(ctx->object, path, pcm_path_element_count(&(ctx->dmRoot)), ctx->imex.dm_dataset);
    when_false_trace(retval, exit, ERROR, "Failed to remove the object: %s", pcm_path_da_get(path));
    SAH_TRACEZ_INFO(ME, "The object: %s removed successfully", pcm_path_da_get(path));
exit:
    return retval;
}

bool pcm_imex_import(pcm_ctx_t* ctx, const pcm_upc_t* data) {
    bool ret_val = false;

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    when_null_trace(data, exit, ERROR, "UPC data is NULL");
    when_true_trace(pcm_svc_state(ctx) != pcm_state_ready, exit, ERROR, "Context's state is not ready");
    when_false_trace(pcm_upc_is_valid(data), exit, ERROR, "UPC data is not valid");

    ret_val = pcm_imex_start_import(ctx, data);
    when_false_trace(ret_val, exit, ERROR, "Failed to start import");

    ret_val = pcm_upc_for_each(data, pcm_imex_for_each_cb, ctx);
    when_false_trace(ret_val, error, ERROR, "Error while looping over UPC dataset");

    ret_val = pcm_imex_finish(ctx);
    when_false_trace(ret_val, error, ERROR, "Failed to finish import");
    SAH_TRACEZ_INFO(ME, "Import finished successfully for the object: %s", amxd_object_get_name(ctx->object, AMXD_OBJECT_NAMED));
exit:
    return ret_val;
error:
    pcm_imex_error(ctx);
    return false;
}

pcm_upc_t* pcm_imex_export(pcm_ctx_t* ctx, bool usersetting, bool changed, amxc_string_t* userflags) {
    pcm_upc_t* data = NULL;
    bool ret_val = false;

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    when_true_trace(pcm_svc_state(ctx) != pcm_state_ready, exit, ERROR, "Context's state is not ready");
    SAH_TRACEZ_INFO(ME, "Exporting data model");

    data = pcm_imex_start_export(ctx, usersetting, userflags);
    when_null_trace(data, exit, ERROR, "Failed to start export");

    ret_val = pcm_dm_for_each(ctx->object, usersetting, changed, pcm_imex_for_each_cb, ctx);
    when_false_trace(ret_val, error, ERROR, "Error while looping over data model");

    ret_val = pcm_imex_finish(ctx);
    when_false_trace(ret_val, error, ERROR, "Failed to finish export");
    SAH_TRACEZ_INFO(ME, "Export finished successfully for the object: %s", amxd_object_get_name(ctx->object, AMXD_OBJECT_NAMED));
exit:
    return data;
error:
    SAH_TRACEZ_ERROR(ME, "Failed exporting data model");
    pcm_imex_error(ctx);
    pcm_upc_destroy(data);
    return NULL;
}

/*Static Functions*/
static bool pcm_imex_finish(pcm_ctx_t* ctx) {
    bool retval = false;

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    switch(pcm_svc_state(ctx)) {
    case pcm_state_exporting:
        SAH_TRACEZ_INFO(ME, "Pcm plug-in '%s' finished export", ctx->info.name);
        retval = true;
        break;
    case pcm_state_importing:
        SAH_TRACEZ_INFO(ME, "Pcm plug-in '%s' finished import", ctx->info.name);
        retval = true;
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Pcm state is not exporting/importing");
        goto exit;
    }

    ctx->state = pcm_state_ready;
    ctx->imex.upc_data = NULL;
    amxc_llist_clean(&ctx->imex.userflags, amxc_string_list_it_free);

exit:
    return retval;
}

static bool pcm_imex_error(pcm_ctx_t* ctx) {
    bool ret_val = false;

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    switch(pcm_svc_state(ctx)) {
    case pcm_state_exporting:
        SAH_TRACEZ_ERROR(ME, "Pcm plug-in '%s' failed export", ctx->info.name);
        ret_val = true;
        break;
    case pcm_state_importing:
        SAH_TRACEZ_ERROR(ME, "Pcm plug-in '%s' failed import", ctx->info.name);
        ret_val = true;
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Pcm state is not exporting/importing");
        goto exit;
    }

    ctx->state = pcm_state_ready;
    ctx->imex.upc_data = NULL;
    amxc_llist_clean(&ctx->imex.userflags, amxc_string_list_it_free);

exit:
    return ret_val;
}

static bool pcm_imex_perform_remove(pcm_ctx_t* ctx, const pcm_path_t* path, amxc_var_t* value) {
    pcm_path_t path_copy;
    bool retval = false;

    pcm_path_initialize(&path_copy);

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    when_null_trace(path, exit, ERROR, "The value path is NULL");
    when_null_trace(value, exit, ERROR, "Value is NULL");

    pcm_path_copy(&path_copy, path);
    if(ctx->state == pcm_state_importing) {
        amxc_var_for_each(var, value) {
            pcm_path_element_push(&path_copy, amxc_var_key(var));
            retval = pcm_imex_remove(ctx, &path_copy);
            pcm_path_element_pop(&path_copy);
            when_false_trace(retval, exit, ERROR, "Failed to delete path '%s'", pcm_path_da_get(&path_copy));
            SAH_TRACEZ_INFO(ME, "Deleted path: '%s'", pcm_path_da_get(&path_copy));
        }

    }

exit:
    pcm_path_cleanup(&path_copy);
    return retval;
}

static bool pcm_imex_for_each_cb(const pcm_path_t* path, amxc_var_t* value, pcm_dataset_t dataset, void* userdata) {
    pcm_ctx_t* ctx = NULL;
    bool retval = false;

    ctx = (pcm_ctx_t*) userdata;
    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    when_null_trace(path, exit, ERROR, "The value path is NULL");
    when_null_trace(value, exit, ERROR, "UPC data is NULL");

    ctx->imex.upc_dataset = dataset;
    ctx->imex.dm_dataset = dataset == pcm_dataset_del ? pcm_dataset_del : pcm_dataset_set;

    SAH_TRACEZ_INFO(ME, "Performing %s of path '%s' (action=%s)", ctx->state == pcm_state_importing ? "import" : "export",
                    pcm_path_da_get(path), dataset == pcm_dataset_set ? "set" : dataset == pcm_dataset_add ? "add" : "delete");

    switch(pcm_svc_state(ctx)) {
    case pcm_state_exporting:
        /* Set variant values */
        retval = pcm_upc_set(ctx->imex.upc_data, path, value, ctx->imex.upc_dataset);
        when_false_trace(retval, exit, ERROR, "Failed performing export of path '%s' (action=%s)", pcm_path_da_get(path),
                         dataset == pcm_dataset_set ? "set" : dataset == pcm_dataset_add ? "add" : "delete");
        break;
    case pcm_state_importing:
        /* Check for delete during import */
        if(dataset == pcm_dataset_del) {
            when_false_status(pcm_imex_is_template(ctx, path), exit, retval = true);
            retval = pcm_imex_perform_remove(ctx, path, value);
            when_false_trace(retval, exit, ERROR, "Failed performing removal");
            goto exit;
        }

        /* Set DataModel values */
        retval = pcm_dm_set(ctx->object, path, pcm_path_element_count(&(ctx->dmRoot)), value, ctx->imex.dm_dataset);
        when_false_trace(retval, exit, ERROR, "Failed performing import of path '%s' (action=%s)", pcm_path_da_get(path),
                         dataset == pcm_dataset_set ? "set" : "add");
        break;

    default:
        SAH_TRACEZ_ERROR(ME, "Pcm state is not exporting/importing");
        break;
    }

exit:
    return retval;
}

static pcm_upc_t* pcm_imex_start_export(pcm_ctx_t* ctx, bool usersetting, amxc_string_t* userflags) {
    pcm_upc_t* data = NULL;

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    when_true_trace(pcm_svc_state(ctx) != pcm_state_ready, exit, ERROR, "Context's state is not ready");

    data = pcm_upc_create(usersetting, userflags);
    when_null_trace(data, exit, ERROR, "Failed to create UPC data");

    ctx->state = pcm_state_exporting;
    ctx->imex.upc_data = data;
    ctx->imex.upc_dataset = pcm_dataset_set;
    ctx->imex.dm_dataset = pcm_dataset_set;
    when_false_trace(pcm_imex_init_userflags(ctx, userflags), exit, ERROR,
                     "Could not initialize the userflags");
    SAH_TRACEZ_INFO(ME, "Pcm plug-in '%s' ready for export", ctx->info.name);

exit:
    return data;
}

static bool pcm_imex_start_import(pcm_ctx_t* ctx, const pcm_upc_t* data) {
    bool ret_val = false;

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    when_null_trace(data, exit, ERROR, "UPC data is NULL");
    when_true_trace(pcm_svc_state(ctx) != pcm_state_ready, exit, ERROR, "Context's state is not ready");

    ctx->state = pcm_state_importing;
    ctx->imex.upc_data = (pcm_upc_t*) data;
    ctx->imex.upc_dataset = pcm_dataset_set;
    ctx->imex.dm_dataset = pcm_dataset_set;

    SAH_TRACEZ_INFO(ME, "Pcm plug-in '%s' ready for import", ctx->info.name);
    ret_val = true;
exit:
    return ret_val;
}

static bool pcm_imex_init_userflags(pcm_ctx_t* ctx, amxc_string_t* userflags) {
    amxc_var_t userflags_list;
    bool retval = false;

    amxc_var_init(&userflags_list);
    amxc_var_set_type(&userflags_list, AMXC_VAR_ID_LIST);

    when_null_trace(ctx, exit, ERROR, "PCM client context is not provided");
    when_null_trace(userflags, exit, ERROR, "Userflags argument is NULL");
    amxc_llist_init(&ctx->imex.userflags);

    amxc_string_csv_to_var(userflags, &userflags_list, NULL);
    amxc_var_for_each(userflag_csv, &userflags_list) {
        amxc_llist_add_string(&ctx->imex.userflags, GET_CHAR(userflag_csv, NULL));
    }
    retval = true;

exit:
    amxc_var_clean(&userflags_list);
    return retval;
}

static bool pcm_imex_is_template(pcm_ctx_t* ctx, const pcm_path_t* path) {
    bool retval = false;
    amxd_object_t* obj = NULL;

    when_null_trace(ctx, exit, ERROR, "PCM client context is not provided");
    when_null_trace(path, exit, ERROR, "Path is NULL");

    obj = pcm_dm_get_object(ctx->object, path, pcm_path_element_count(&(ctx->dmRoot)), false);
    when_null_trace(obj, exit, ERROR, "Could not find the object [%s]", pcm_path_da_get(path));
    if(amxd_object_template == amxd_object_get_type(obj)) {
        retval = true;
    }

exit:
    return retval;
}