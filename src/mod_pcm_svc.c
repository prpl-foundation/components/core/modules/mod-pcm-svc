/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_path.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_pcm_svc.h"
#include "mod_pcm_imex.h"
#include "mod_pcm_rpc.h"
#include "mod_pcm_reg.h"
#include "mod_pcm_inst.h"
#include "mod_pcm_ctx.h"

#define STR_EMPTY(x) (x) == NULL || (x)[0] == '\0'
#define COPT_STORAGE_DIR "storage-path"
#define COPT_NAME "name"
#define ME "mod_pcm_svc"

typedef struct {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxc_var_t deleted_instances;
    amxc_var_t run_time_instances;
    bool pcm_available;
    bool config_loaded;
} pcm_srv_t;

static pcm_srv_t module;
static const char* default_export = "PcmExport";
static const char* default_import = "PcmImport";

static bool pcm_svc_is_pcm_manager_available(void);
static int pcm_ctx_init(pcm_ctx_t* ctx, amxd_dm_t* dm);
static bool pcm_svc_has_upc_params(amxd_object_t* object, amxc_var_t* params);
static void pcm_svc_param_changed(const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv);
static void pcm_svc_instance_added(const char* const sig_name,
                                   const amxc_var_t* const data,
                                   void* const priv);
static void pcm_svc_instance_removed(const char* const sig_name,
                                     const amxc_var_t* const data,
                                     void* const priv);
static amxd_status_t pcm_svc_register_import_function(amxd_object_fn_t impl);
static amxd_status_t pcm_svc_register_export_function(amxd_object_fn_t impl);
static amxd_status_t pcm_svc_init(void);
static void pcm_svc_cleanup(void);
static bool pcm_svc_has_upc_flag(amxd_param_t* param);

static void pcm_available_cb(UNUSED const char* signame,
                             UNUSED const amxc_var_t* const data,
                             UNUSED void* const priv) {
    amxb_bus_ctx_t* pcm_ctx = NULL;

    pcm_ctx = amxb_be_who_has("PersistentConfiguration.");
    when_null_trace(pcm_ctx, exit, ERROR, "No PersistentConfiguration bus context found");
    SAH_TRACEZ_INFO(ME, "PersistentConfiguration available");

    module.pcm_available = true;
    when_false_trace(module.config_loaded, exit, WARNING, "The config odl hasn't been loaded yet");
    pcm_svc_init();

exit:
    return;
}

amxd_dm_t* pcm_svc_dm(void) {
    return module.dm;
}

amxo_parser_t* pcm_svc_parser(void) {
    return module.parser;
}

amxc_var_t* pcm_svc_deleted_instances(void) {
    return &module.deleted_instances;
}

pcm_state_t pcm_svc_state(pcm_ctx_t* ctx) {
    pcm_state_t ret_val = pcm_state_unknown;

    when_null_trace(ctx, exit, ERROR, "Bus context is NULL");
    ret_val = ctx->state;
exit:
    return ret_val;
}

char* pcm_svc_get_directory(void) {
    char* resolved_dir = NULL;
    amxo_parser_t* parser = module.parser;
    amxc_string_t dir;
    char* storage_dir = NULL;

    when_null(parser, exit);
    storage_dir = amxc_var_dyncast(cstring_t, GET_ARG(&parser->config, COPT_STORAGE_DIR));

    when_str_empty_trace(storage_dir, exit, ERROR, "Failed to resolve the directory path");
    if(storage_dir[strlen(storage_dir) - 1] == '/') {
        storage_dir[strlen(storage_dir) - 1] = '\0';
    }

    amxc_string_init(&dir, 0);
    amxc_string_setf(&dir, "%s/pcm", storage_dir);
    amxc_string_resolve(&dir, &parser->config);
    resolved_dir = amxc_string_take_buffer(&dir);

exit:
    free(storage_dir);
    return resolved_dir;
}

void app_start(UNUSED const char* const sig_name,
               UNUSED const amxc_var_t* const data,
               UNUSED void* const priv) {

    SAH_TRACEZ_INFO(ME, "Config odl is loaded");
    module.config_loaded = true;
    when_false_trace(module.pcm_available, exit, WARNING, "PCM is not yet available");
    pcm_svc_init();

exit:
    return;
}

int _main(int reason,
          amxd_dm_t* dm,
          amxo_parser_t* parser) {
    switch(reason) {
    case 0:         // START
        module.parser = parser;
        module.dm = dm;
        module.pcm_available = false;
        module.config_loaded = false;

        if(!pcm_svc_is_pcm_manager_available()) {
            int rv = -1;

            SAH_TRACEZ_ERROR(ME, "PCM manager is not availabe");
            amxb_wait_for_object("PersistentConfiguration.");
            rv = amxp_slot_connect_filtered(NULL, "^wait:PersistentConfiguration\\.$", NULL, pcm_available_cb, NULL);
            when_failed_trace(rv, exit, ERROR, "Failed to wait for pcm-manager to be available");
            SAH_TRACEZ_WARNING(ME, "Waiting for pcm-manager");
            break;
        }
        break;
    case 1:         // STOP
        pcm_svc_cleanup();
        break;
    default:
        break;
    }

exit:
    return 0;
}

/* Static Functions */

static bool pcm_svc_is_pcm_manager_available(void) {
    bool rv = false;
    amxb_bus_ctx_t* bus = NULL;
    bus = pcm_reg_get_pcm_manager_ctx();
    if(bus != NULL) {
        rv = true;
        module.pcm_available = true;
    }
    return rv;
}

static int pcm_ctx_init(pcm_ctx_t* ctx, amxd_dm_t* dm) {
    int ret = -1;
    amxo_parser_t* parser = NULL;
    amxc_var_t* pcm_svc_config = NULL;
    amxc_string_t backup_files;
    amxc_string_t objects_to_register;
    amxc_string_t userflags_csv;
    amxc_var_t data;
    char* directory = NULL;
    amxc_string_t file_path;
    amxb_bus_ctx_t* bus_ctx;
    uint32_t current_access = 0;

    amxc_string_init(&objects_to_register, 0);
    amxc_string_init(&backup_files, 0);
    amxc_string_init(&userflags_csv, 0);
    amxc_string_init(&file_path, 0);
    amxc_var_init(&data);

    when_null_trace(ctx, exit, ERROR, "Invalid argument");
    when_null_trace(dm, exit, ERROR, "Invalid argument");

    parser = pcm_svc_parser();
    pcm_svc_config = amxc_var_get_key(&parser->config, "pcm_svc_config", AMXC_VAR_FLAG_DEFAULT);

    ctx->info.name = GET_CHAR(&parser->config, COPT_NAME);
    when_str_empty(ctx->info.name, exit);

    ctx->info.rpcImport = GET_CHAR(pcm_svc_config, "Import");
    if(STR_EMPTY(ctx->info.rpcImport)) {
        ctx->info.rpcImport = default_import;
    }

    ctx->info.rpcExport = GET_CHAR(pcm_svc_config, "Export");
    if(STR_EMPTY(ctx->info.rpcExport)) {
        ctx->info.rpcExport = default_export;
    }

    ctx->info.rpcPostExport = GET_CHAR(pcm_svc_config, "PostExport");
    ctx->info.priority = GETP_UINT32(pcm_svc_config, "Priority");

    directory = pcm_svc_get_directory();
    when_str_empty_trace(directory, exit, ERROR, "Failed to resolve storage path");

    amxc_string_setf(&file_path, "%s/%s-del.json", directory, ctx->info.name);
    ctx->deleted_instances = amxc_string_take_buffer(&file_path);
    amxc_string_setf(&file_path, "%s/%s-add.json", directory, ctx->info.name);
    ctx->added_instances = amxc_string_take_buffer(&file_path);

    bus_ctx = pcm_reg_get_pcm_manager_ctx();
    when_null_trace(bus_ctx, exit, ERROR, "Failed to get bus ctx");

    amxc_var_new(&ctx->supported_flags);
    amxc_var_set_type(ctx->supported_flags, AMXC_VAR_ID_LIST);
    current_access = bus_ctx->access;
    amxb_set_access(bus_ctx, AMXB_PROTECTED);
    ret = amxb_get(bus_ctx, "PersistentConfiguration.SupportedUserflags", 0, &data, 5);
    amxb_set_access(bus_ctx, current_access);
    when_failed_trace(ret, exit, ERROR, "Failed to get the SupportedUserflags %s [%d]", amxd_status_string(ret), ret);
    amxc_string_set(&userflags_csv, GETP_CHAR(&data, "0.0.SupportedUserflags"));

    when_str_empty_trace(amxc_string_get(&userflags_csv, 0), exit, ERROR, "SupportedUserflags is empty");
    amxc_string_csv_to_var(&userflags_csv, ctx->supported_flags, NULL);

    amxc_string_set(&backup_files, GET_CHAR(pcm_svc_config, "BackupFiles"));
    amxc_llist_init(&(ctx->backup_files));
    if(AMXC_STRING_SPLIT_OK != amxc_string_split_to_llist(&backup_files, &(ctx->backup_files), ',')) {
        goto exit;
    }

    ctx->info.objects = GET_CHAR(pcm_svc_config, "Objects");
    when_str_empty(ctx->info.objects, exit);
    SAH_TRACEZ_INFO(ME, "Found Objects '%s'", ctx->info.objects);

    amxc_llist_init(&(ctx->registered_objects));
    amxc_string_set(&objects_to_register, ctx->info.objects);

    if(AMXC_STRING_SPLIT_OK != amxc_string_split_to_llist(&objects_to_register, &(ctx->registered_objects), ',')) {
        goto exit;
    }


    pcm_path_initialize(&(ctx->dmRoot));
    if(!pcm_path_set(&(ctx->dmRoot), ctx->info.objects)) {
        SAH_TRACEZ_ERROR(ME, "Failed to set data model root path");
        goto exit;
    }
    ret = 0;

exit:
    amxc_var_clean(&data);
    amxc_string_clean(&userflags_csv);
    amxc_string_clean(&objects_to_register);
    amxc_string_clean(&backup_files);
    amxc_string_clean(&file_path);
    free(directory);
    return ret;

}

static bool pcm_svc_has_upc_params(amxd_object_t* object, amxc_var_t* params) {
    bool retval = false;
    amxd_param_t* param_def = NULL;

    when_null_trace(params, exit, ERROR, "params is not provided");
    when_null_trace(object, exit, ERROR, "Failed to get object %s", amxd_object_get_path(object, AMXD_OBJECT_NAMED));

    amxc_var_for_each(param, params) {
        const char* param_name = amxc_var_key(param);
        param_def = amxd_object_get_param_def(object, param_name);
        if(pcm_svc_has_upc_flag(param_def)) {
            retval = true;
            break;
        }
    }

exit:
    return retval;
}

static bool pcm_svc_has_upc_flag(amxd_param_t* param) {
    bool ret = false;
    pcm_ctx_t* ctx = pcm_ctx_get();

    when_null_trace(param, exit, ERROR, "param is not provided");
    when_null_trace(ctx, exit, ERROR, "Could not retrieve the ctx values");
    amxc_var_for_each(flag, ctx->supported_flags) {
        if(amxd_param_has_flag(param, GET_CHAR(flag, NULL))) {
            ret = true;
            break;
        }
    }

exit:
    return ret;
}

static void pcm_svc_param_changed(const char* const sig_name,
                                  const amxc_var_t* const data,
                                  UNUSED void* const priv) {
    (void) sig_name;
    amxd_object_t* object = amxd_dm_signal_get_object(pcm_svc_dm(), data);
    amxd_param_t* param_def = NULL;
    amxc_var_t* params = GET_ARG(data, "parameters");

    SAH_TRACEZ_INFO(ME, "Received SIGNAL [%s]", sig_name);

    amxc_var_for_each(param, params) {
        const char* param_name = amxc_var_key(param);
        param_def = amxd_object_get_param_def(object, param_name);
        if(pcm_svc_has_upc_flag(param_def)) {
            SAH_TRACEZ_INFO(ME, "Set flag upc_changed - param_name:[%s]", param_name);
            amxd_param_set_flag(param_def, "upc_changed");
        }
    }
}

/*
 * Translates the path of a changed pcm_svc_instance to a object definition.
 * For example: WiFi.Radio.1.ScanResults.SurroundingChannels.7.Accesspoint. -> WiFi.Radio.ScanResults.SurroundingChannels.Accesspoint.
 */
static amxd_object_t* get_definition_object(const char* path) {
    char* definition_path = NULL;
    amxd_object_t* def_obj = NULL;
    amxd_path_t obj_path;

    amxd_path_init(&obj_path, NULL);
    amxd_path_setf(&obj_path, true, "%s", path);

    definition_path = amxd_path_get_supported_path(&obj_path);
    def_obj = amxd_dm_findf(pcm_svc_dm(), "%s", definition_path);
    when_null_trace(def_obj, exit, ERROR, "Failed to get definition out of '%s'", definition_path);

exit:
    amxd_path_clean(&obj_path);
    free(definition_path);
    return def_obj;
}

static void pcm_svc_instance_added(const char* const sig_name,
                                   const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    const char* path = NULL;
    const char* name = NULL;
    const char* primkey = NULL;
    const char* primkeyvalue = NULL;
    amxd_object_t* object = NULL;
    amxc_var_t* keys = NULL;
    amxc_var_t* params = NULL;
    pcm_ctx_t* ctx = NULL;
    amxc_htable_it_t* it = NULL;
    amxd_param_t* param_def = NULL;
    bool has_upc = false;

    ctx = pcm_ctx_get();
    when_null(ctx, exit);

    (void) sig_name;
    SAH_TRACEZ_INFO(ME, "Received SIGNAL [%s]", sig_name);

    path = GET_CHAR(data, "object");
    name = GET_CHAR(data, "name");

    object = amxd_dm_findf(pcm_svc_dm(), "%s%s", path, name);
    when_null_trace(object, exit, ERROR, "Failed to get instance out of '%s%s'", path, name);

    keys = GET_ARG(data, "keys");
    when_null_trace(keys, exit, ERROR, "Failed to find keys");

    it = amxc_htable_get_first(amxc_var_constcast(amxc_htable_t, keys));
    if(it) {
        primkey = amxc_htable_it_get_key(it);
        primkeyvalue = amxc_var_constcast(cstring_t, amxc_var_from_htable_it(it));
    }

    SAH_TRACEZ_INFO(ME, "path:[%s] name:[%s] primkey:[%s] primkeyvalue:[%s]", path, name, primkey, primkeyvalue);

    params = GET_ARG(data, "parameters");
    has_upc = pcm_svc_has_upc_params(object, params);
    if(has_upc) {
        SAH_TRACEZ_INFO(ME, "Add new instance in the run-time instances list - path:[%s] name:[%s]", path, name);
        pcm_inst_add(&module.run_time_instances, path, name, "upc", primkey, primkeyvalue);
        SAH_TRACEZ_INFO(ME, "Write all added instances to file - file_path:[%s]", ctx->added_instances);
        pcm_inst_write(ctx->added_instances, &module.run_time_instances);

        SAH_TRACEZ_INFO(ME, "Remove new instance from deleted instances - path:[%s] name:[%s]", path, name);
        pcm_inst_remove(&module.deleted_instances, path, name);
        SAH_TRACEZ_INFO(ME, "Write all deleted instances to file - file_path:[%s]", ctx->deleted_instances);
        pcm_inst_write(ctx->deleted_instances, &module.deleted_instances);

    }

    amxc_var_for_each(param, params) {
        const char* param_name = amxc_var_key(param);
        param_def = amxd_object_get_param_def(object, param_name);
        if(pcm_svc_has_upc_flag(param_def)) {
            SAH_TRACEZ_INFO(ME, "Set flag upc_changed - param_name:[%s]", param_name);
            amxd_param_set_flag(param_def, "upc_changed");
        }
    }

exit:
    return;
}

static void pcm_svc_instance_removed(const char* const sig_name,
                                     const amxc_var_t* const data,
                                     UNUSED void* const priv) {
    pcm_ctx_t* ctx = NULL;
    amxc_var_t* keys = NULL;
    amxc_htable_it_t* it = NULL;
    amxc_var_t* params = NULL;
    amxd_object_t* definition_obj = NULL;
    const char* path = NULL;
    const char* name = NULL;
    const char* primkey = NULL;
    const char* primkeyvalue = NULL;

    ctx = pcm_ctx_get();
    when_null(ctx, exit);

    (void) sig_name;
    SAH_TRACEZ_INFO(ME, "Received SIGNAL [%s]", sig_name);

    path = GET_CHAR(data, "object");
    name = GET_CHAR(data, "name");
    definition_obj = get_definition_object(GET_CHAR(data, "path"));

    keys = GET_ARG(data, "keys");
    when_null_trace(keys, exit, ERROR, "Failed to find keys");

    it = amxc_htable_get_first(amxc_var_constcast(amxc_htable_t, keys));
    if(it) {
        primkey = amxc_htable_it_get_key(it);
        primkeyvalue = amxc_var_constcast(cstring_t, amxc_var_from_htable_it(it));
    }

    SAH_TRACEZ_INFO(ME, "path:[%s] name:[%s] primkey:[%s] primkeyvalue:[%s]", path, name, primkey, primkeyvalue);
    params = GET_ARG(data, "parameters");

    if(pcm_svc_has_upc_params(definition_obj, params)) {
        if(!pcm_svc_is_run_time(path, name, &module.run_time_instances)) {
            SAH_TRACEZ_INFO(ME, "Add instance to deleted instances - path:[%s] name:[%s]", path, name);
            pcm_inst_add(&module.deleted_instances, path, name, "upc", primkey, primkeyvalue);
            SAH_TRACEZ_INFO(ME, "Write all deleted instances to file - file_path:[%s]", ctx->deleted_instances);
            pcm_inst_write(ctx->deleted_instances, &module.deleted_instances);
        }
        SAH_TRACEZ_INFO(ME, "Remove new instance from run-time instances - path:[%s] name:[%s]", path, name);
        pcm_inst_remove(&module.run_time_instances, path, name);
        SAH_TRACEZ_INFO(ME, "Write all added instances to file - file_path:[%s]", ctx->added_instances);
        pcm_inst_write(ctx->added_instances, &module.run_time_instances);

    }

exit:
    return;
}

static amxd_status_t pcm_svc_register_import_function(amxd_object_fn_t impl) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxd_function_t* function = NULL;
    pcm_ctx_t* ctx = pcm_ctx_get();

    when_null(ctx, exit);

    when_failed(amxd_function_new(&function,
                                  default_import,
                                  AMXC_VAR_ID_NULL,
                                  impl), error);

    when_failed(amxd_function_set_attr(function, amxd_fattr_protected, true), error);
    when_failed(amxd_function_new_arg(function, "data", AMXC_VAR_ID_HTABLE, NULL), error);
    when_failed(amxd_function_arg_is_attr_set(function, "data", amxd_aattr_in), error);
    when_failed(amxd_function_arg_is_attr_set(function, "data", amxd_aattr_mandatory), error);

    rc = amxd_object_add_function(ctx->object, function);
    when_failed(rc, error);

exit:
    return rc;

error:
    amxd_function_delete(&function);
    return rc;
}

static amxd_status_t pcm_svc_register_export_function(amxd_object_fn_t impl) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxd_function_t* function = NULL;
    amxc_var_t def_val;
    pcm_ctx_t* ctx = pcm_ctx_get();

    when_null(ctx, exit);

    when_failed(amxd_function_new(&function,
                                  default_export,
                                  AMXC_VAR_ID_NULL,
                                  impl), error);

    amxc_var_init(&def_val);

    when_failed(amxd_function_set_attr(function, amxd_fattr_protected, true), error);

    amxc_var_set(bool, &def_val, false);
    when_failed(amxd_function_new_arg(function, "usersetting", AMXC_VAR_ID_BOOL, &def_val), error);
    amxd_function_arg_set_attr(function, "usersetting", amxd_aattr_in, true);

    amxc_var_set(bool, &def_val, true);
    when_failed(amxd_function_new_arg(function, "changed", AMXC_VAR_ID_BOOL, &def_val), error);
    amxd_function_arg_set_attr(function, "changed", amxd_aattr_in, true);

    amxc_var_set(cstring_t, &def_val, "upc,usersetting");
    when_failed(amxd_function_new_arg(function, "Flags", AMXC_VAR_ID_CSTRING, &def_val), error);
    amxd_function_arg_set_attr(function, "Flags", amxd_aattr_in, true);

    when_failed(amxd_function_new_arg(function, "data", AMXC_VAR_ID_HTABLE, NULL), error);
    amxd_function_arg_set_attr(function, "data", amxd_aattr_out, true);

    rc = amxd_object_add_function(ctx->object, function);
    when_failed(rc, error);

exit:
    amxc_var_clean(&def_val);
    return rc;

error:
    amxd_function_delete(&function);
    return rc;
}

static amxd_status_t pcm_svc_init(void) {
    amxd_status_t status = amxd_status_unknown_error;
    size_t list_size = 0;
    size_t index = 0;
    amxd_dm_t* dm = NULL;
    pcm_ctx_t* ctx = NULL;
    amxb_bus_ctx_t* plugin_ctx = NULL;
    const char* rpcPostExport = NULL;

    when_failed(pcm_ctx_create(), exit);
    ctx = pcm_ctx_get();
    dm = pcm_svc_dm();

    when_failed_trace(pcm_ctx_init(ctx, dm), exit, ERROR, "Failed to initialize the ctx");

    amxc_var_init(&module.deleted_instances);
    amxc_var_set_type(&module.deleted_instances, AMXC_VAR_ID_LIST);

    amxc_var_init(&module.run_time_instances);
    amxc_var_set_type(&module.run_time_instances, AMXC_VAR_ID_LIST);

    SAH_TRACEZ_INFO(ME, "Read all previous deleted upc/usersetting instances from file '%s'", ctx->deleted_instances);
    pcm_inst_read(ctx->deleted_instances, &module.deleted_instances);

    SAH_TRACEZ_INFO(ME, "Read all previous run-time added upc/usersetting instances from file '%s'", ctx->added_instances);
    pcm_inst_read(ctx->added_instances, &module.run_time_instances);

    SAH_TRACEZ_INFO(ME, "Create subscription for changed-added-removed");
    amxp_slot_connect(&dm->sigmngr, "dm:object-changed", NULL, pcm_svc_param_changed, NULL);
    amxp_slot_connect(&dm->sigmngr, "dm:instance-added", NULL, pcm_svc_instance_added, NULL);
    amxp_slot_connect(&dm->sigmngr, "dm:instance-removed", NULL, pcm_svc_instance_removed, NULL);

    list_size = amxc_llist_size(&(ctx->registered_objects));
    while(index < list_size) {
        amxc_llist_it_t* iter = NULL;
        amxc_string_t* object = NULL;
        amxc_string_t* file = NULL;

        iter = amxc_llist_get_at(&(ctx->backup_files), index);
        ctx->info.file = NULL;
        if(iter != NULL) {
            file = amxc_container_of(iter, amxc_string_t, it);
            ctx->info.file = amxc_string_get(file, 0);
        }

        iter = amxc_llist_get_at(&(ctx->registered_objects), index);
        object = amxc_container_of(iter, amxc_string_t, it);
        ctx->object = amxd_dm_findf(dm, "%s", amxc_string_get(object, 0));
        when_null_trace(ctx->object, exit, ERROR, "Failed to retrieve data model root object - %s", amxc_string_get(object, 0));

        status = pcm_svc_register_import_function(pcm_rpc_import);
        when_failed_trace(status, post, ERROR, "Failed to resolve import function");

        status = pcm_svc_register_export_function(pcm_rpc_export);
        when_failed_trace(status, post, ERROR, "Failed to resolve export function");

        ctx->state = pcm_state_initialized;
        SAH_TRACEZ_INFO(ME, "Initialized PCM for plug-in '%s'", ctx->info.name);

        status = pcm_reg_register();
        when_failed(status, post);
        SAH_TRACEZ_INFO(ME, "Registered PCM for plug-in '%s'", ctx->info.name);
        index++;
    }

post:
    /* PCM has loaded whatever data it had stored. Call PostExport if it's
     * registered */
    rpcPostExport = ctx->info.rpcPostExport;
    when_null(rpcPostExport, exit);
    plugin_ctx = amxb_be_who_has(ctx->object->name);
    when_null(plugin_ctx, exit);
    amxb_call(plugin_ctx, ctx->object->name, rpcPostExport, NULL, NULL, 1);

exit:
    return status;
}

static void pcm_svc_cleanup(void) {
    pcm_ctx_t* ctx = NULL;
    ctx = pcm_ctx_get();
    when_null_trace(ctx, exit, ERROR, "ctx dataset is empty");
    free(ctx->added_instances);
    free(ctx->deleted_instances);

    amxc_llist_for_each(it, &(ctx->registered_objects)) {
        amxc_string_t* object = amxc_llist_it_get_data(it, amxc_string_t, it);
        pcm_reg_perform_unregister(amxc_string_get(object, 0));
    }

exit:
    module.dm = NULL;
    module.parser = NULL;
    amxc_var_clean(&module.deleted_instances);
    amxc_var_clean(&module.run_time_instances);
    pcm_ctx_cleanup();
}

