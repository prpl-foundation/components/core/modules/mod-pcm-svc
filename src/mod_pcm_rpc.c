/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "mod_pcm_def.h"
#include "mod_pcm_ctx.h"

#include "mod_pcm_upc.h"
#include "mod_pcm_imex.h"
#include "mod_pcm_rpc.h"

#define ME "mod_pcm_svc"

amxd_status_t pcm_rpc_import(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    pcm_ctx_t* ctx = pcm_ctx_get();
    pcm_upc_t* data = NULL;

    when_null(ctx, exit);
    when_null(args, exit);

    if(ctx->state == pcm_state_registering) {
        /*
         * This is registration test performed by the PCM plug-in.
         * It verifies that the RPC is valid and that we receive all arguments.
         */
        return amxd_status_ok;
    }

    /* Retrieve arguments */
    data = (pcm_upc_t*) GET_ARG(args, "data");
    when_null(data, exit);

    ctx->object = object;
    SAH_TRACEZ_INFO(ME, "Importing UPC data");

    when_false_trace(pcm_imex_import(ctx, data), exit, ERROR, "Importing Failed");
    rv = amxd_status_ok;
exit:
    return rv;
}

amxd_status_t pcm_rpc_export(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    pcm_upc_t* data = NULL;
    amxd_status_t rc = amxd_status_unknown_error;
    bool usersetting = false;
    bool changed = false;
    amxc_string_t userflags;
    pcm_ctx_t* ctx = pcm_ctx_get();

    amxc_string_init(&userflags, 0);

    when_null_trace(ctx, exit, ERROR, "Could not retrieve the CTX content");
    when_null_trace(args, exit, ERROR, "Invalid argument");

    if(ctx->state == pcm_state_registering) {
        /*
         * This is registration test performed by the PCM plug-in.
         * It verifies that the RPC is valid and that we receive all arguments.
         */
        return amxd_status_ok;
    }

    usersetting = GET_BOOL(args, "usersetting");
    changed = GET_BOOL(args, "changed");

    amxc_string_set(&userflags, GET_CHAR(args, "Flags"));

    ctx->object = object;
    SAH_TRACEZ_INFO(ME, "Exporting data model - usersetting:[%d] changed:[%d]", usersetting, changed);
    data = pcm_imex_export(ctx, usersetting, changed, &userflags);
    when_null(data, exit);

    amxc_var_t* arg_data = amxc_var_add_key(amxc_htable_t, args, "data", NULL);
    when_null_status(arg_data, fail, rc = amxd_status_missing_key);
    when_failed(amxc_var_copy(arg_data, data), fail);

    rc = amxd_status_ok;

fail:
    pcm_upc_destroy(data);
exit:
    amxc_string_clean(&userflags);
    return rc;
}
