/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_PCM_DEF__)
#define __MOD_PCM_DEF__

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*
 * VERSION
 */
#define PCM_VERSION_MAJOR      1
#define PCM_VERSION_MINOR      0

/*
 * PATH
 */
typedef struct _pcm_path_t {
    bool dirty;
    amxc_string_t full;
    amxc_llist_t expl;
    int num_elem;
} pcm_path_t;

/*
 * UPC
 */
typedef amxc_var_t pcm_upc_t;

/*
 * DATASET
 */
typedef enum {
    pcm_dataset_set = 0,                              /* Set existing data */
    pcm_dataset_add,                                  /* Add new data */
    pcm_dataset_del,                                  /* Delete data */
} pcm_dataset_t;
#define pcm_dataset_max    (pcm_dataset_del + 1)      /* Last pcm_dataset_t plus one */


/**
 * VERSION
 */
typedef enum {
    pcm_version_unsupported = 0,
    pcm_version_supported,
    pcm_version_supported_partial,
} pcm_version_t;

/**
 * STATE
 */
typedef enum {
    pcm_state_unknown = 0,
    pcm_state_initialized,
    //pcm_state_schemeLoaded,
    //pcm_state_mappingsLoaded,
    pcm_state_registering,
    pcm_state_exporting,
    pcm_state_importing,
    pcm_state_ready,
    pcm_state_error
} pcm_state_t;

/**
 * CTX
 */
typedef struct _pcm_ctx_t {
    pcm_state_t state;
    struct {
        const char* name;                /* Name of the plug-in (mandatory) */
        const char* objects;             /* Comma separated list of objects which need to be registered with pcm_manager (mandatory) */
        const char* rpcImport;           /* Name of the plug-in's import RPC (mandatory) */
        const char* rpcExport;           /* Name of the plug-in's export RPC (mandatory) */
        const char* rpcPostExport;       /* Name of the plug-in's post export RPC (optional) */
        const char* rpcConfig;           /* Name of the plug-in's config RPC (mandatory) */
        const char* file;                /* Name of the alterante backup file (mandatory) */
        uint32_t priority;               /* Priority number to determine the order the plugin in loaded (default 0) */
    } info;
    pcm_path_t dmRoot;
    amxc_llist_t registered_objects;
    amxc_llist_t backup_files;
    amxd_object_t* object;
    amxc_var_t* supported_flags;
    char* deleted_instances;
    char* added_instances;
    struct {
        pcm_upc_t* upc_data;
        pcm_dataset_t upc_dataset;
        pcm_dataset_t dm_dataset;
        amxc_llist_t userflags;
    } imex;
    void* userdata;
} pcm_ctx_t;

/**
 * @brief get the current pcm version
 *
 * @param string string output
 * @param size size of the string
 * @return true on success
 * @return false on failure
 */
bool pcm_version_get(char* string, size_t size);

/**
 * @brief Parse the version into major and minor
 *
 * @param version the string to parse
 * @param major the first number of the version
 * @param minor the second number of the version
 * @return true on success
 * @return false on failure
 */
bool pcm_version_parse(const char* version, unsigned int* major, unsigned int* minor);

/**
 * @brief Check or current pcm version is supported
 *
 * @param major the first number of the version
 * @param minor the second number of the version
 * @return pcm_version_t version supported status
 */
pcm_version_t pcm_version_verify(unsigned int major, unsigned int minor);

/**
 * @brief Save the path to the filesystem (recursive mkdir)
 *
 * @param path the path to save (can be a directory or a filepath)
 * @param mode permission bits
 * @return 0 on success
 * @return -1 if an error occured
 */
int pcm_mk_path(const char* path, const mode_t mode);

#ifndef UNUSED
#define UNUSED  __attribute__((unused))
#endif

#define PARAM_ATTR(param, attr_name) \
    amxc_var_dyncast(bool,                                        \
                     amxc_var_get_path(param,                     \
                                       attr_name,                 \
                                       AMXC_VAR_FLAG_DEFAULT))

#ifdef __cplusplus
}
#endif

#endif // __MOD_PCM_DEF__
