/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_PCM_PATH_H__)
#define __MOD_PCM_PATH_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

#include "mod_pcm_def.h"

#define PATH_SEP            '.'
#define PATH_QUOTE          '"'
#define PATH_ESCAPE         '\\'

/**
 * @brief Create and initialize a path
 *
 * @return path upon success, NULL otherwise
 */
pcm_path_t* pcm_path_create(void);

/**
 * @brief Clean up and destroy a path
 *
 * @param path the path to destroy
 */
void pcm_path_destroy(pcm_path_t* path);

/**
 * @brief Initialize a path
 *
 * @param path the path to initialize
 * @return true upon success, false otherwise
 */
bool pcm_path_initialize(pcm_path_t* path);

/**
 * @brief Clean up a path
 *
 * @param path the path to clean up
 */
void pcm_path_cleanup(pcm_path_t* path);

/**
 * @brief Set the path
 *
 * @param path the path to set
 * @param new_path the new value for the path
 * @return true upon success, false otherwise
 */
bool pcm_path_set(pcm_path_t* path, const char* new_path);

/**
 * @brief Get direct access to the path
 *
 * @details
 * Note: The returned value must not be altered nor free()'d.
 *
 * @param path the path to get
 * @return a pointer to the path upon success, NULL otherwise
 */
const char* pcm_path_da_get(const pcm_path_t* path);

/**
 * @brief Copy a path
 *
 * @param dst the destination path
 * @param src the source path
 * @return true upon success, false otherwise
 */
bool pcm_path_copy(pcm_path_t* dst, const pcm_path_t* src);

/**
 * @brief Compare two paths
 *
 * @details
 * Each corresponding element of both paths must be equal.
 *
 * @param a the first path
 * @param b the second path
 * @return an integer like strcmp() upon success, INT32_MAX otherwise
 */
int pcm_path_compare(const pcm_path_t* a, const pcm_path_t* b);

/**
 * @brief Compare two partial paths
 *
 * @details
 * Both paths must not be of equal length, but they must both contain
 * the required amount of elements that fit the index range.
 * If either 'start_at' or 'end_at' is negative, indicating an element from the end,
 * then path 'a' will define the actual start or end index.
 * Each corresponding element of both partial paths must be equal.
 *
 * @param a the first path
 * @param b the second path
 * @param start_at the start index
 * @param end_at the end index
 * @return an integer like strcmp() upon success, INT32_MAX otherwise
 */
int pcm_path_comparePartial(const pcm_path_t* a, const pcm_path_t* b, int start_at, int end_at);

/**
 * @brief Push an element to the end of a path
 *
 * @param path the path
 * @param element the new element
 * @return true upon success, false otherwise
 */
bool pcm_path_element_push(pcm_path_t* path, const char* element);

/**
 * @brief Push the root element to the beginning of a path
 *
 * @param path the path
 * @param element the name of the root element
 * @return true upon success, false otherwise
 */
bool pcm_path_element_push_root(pcm_path_t* path, const char* element);

/**
 * @brief Pop the last element from a path
 *
 * @param path the path
 * @return true upon success, false otherwise
 */
bool pcm_path_element_pop(pcm_path_t* path);

/**
 * @brief Pop the first root element from a path
 *
 * @param path the path
 * @return true upon success, false otherwise
 */
bool pcm_path_element_pop_root(pcm_path_t* path);

/**
 * @brief Count the number of elements in a path
 *
 * @param path the path
 * @return the number of elements upon success, -1 otherwise
 */
int pcm_path_element_count(const pcm_path_t* path);

/**
 * @brief Get an element of a path
 *
 * @details
 * Note: The returned value is allocated on the heap and
 * must be free()'d after usage.
 *
 * @param path the path
 * @param at the element's index
 * @return a pointer to the element upon success, NULL otherwise
 */
const char* pcm_path_element_da_get(const pcm_path_t* path, int at);

#ifdef __cplusplus
}
#endif

#endif // __MOD_PCM_PATH_H__
