/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__mod_pcm_svc__)
#define __mod_pcm_svc__

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include "mod_pcm_def.h"

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Get the directory where the deleted files are located
 *
 * @return the directory
 */
char* pcm_svc_get_directory(void);

/**
 * @brief Get the root datamodel structure
 *
 * @return pointer to the dm struct
 */
amxd_dm_t* pcm_svc_dm(void);

/**
 * @brief Get the odl parser structure
 *
 * @return pointer to the parser struct
 */
amxo_parser_t* pcm_svc_parser(void);

/**
 * @brief Get the deleted instances list
 *
 * @details
 * This list is loaded into memory on init
 * and updated when an instance is deleted.
 * It is used to keep track of the deleted instances,
 * as the amxrt plugin is only keeping track of the actual datamodel.
 *
 * @return the deleted instance list
 */
amxc_var_t* pcm_svc_deleted_instances(void);

/**
 * @brief Get the context's current state
 *
 * @param ctx the pcm module context
 * @return the context's current state upon success, pcm_state_unknown otherwise
 */
pcm_state_t pcm_svc_state(pcm_ctx_t* ctx);

/**
 * @brief the main function of the module
 *
 * @param reason the start or stop reason
 * @param dm the root datamodel structure
 * @param parser the odl parser structure
 * @return 0 in all cases
 */
int _main(int reason,
          amxd_dm_t* dm,
          amxo_parser_t* parser);

void app_start(const char* const sig_name,
               const amxc_var_t* const data,
               void* const priv);

#ifdef __cplusplus
}
#endif

#endif // __mod_pcm_svc__
