/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_PCM_DM_H__)
#define __MOD_PCM_DM_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include "mod_pcm_def.h"
#include "mod_pcm_path.h"
#include <amxd/amxd_transaction.h>

/**
 * @brief Callback prototype to be used with pcm_dm_for_each()
 *
 * @details
 * When reporting parameters that belong to an added instance, as parent or (grand)*grandparent,
 * the 'dataset' will be pcm_dataset_add instead of pcm_dataset_set.
 * When reporting a deleted instance, the 'dataset' will be pcm_dataset_del and
 * the path will point to the deleted instance instead of a parameter.
 *
 * @param path the path up to a parameter
 * @param value the parameter's value
 * @param dataset the dataset of the parameter
 * @param userdata user data
 * @return true upon success, false otherwise
 */
typedef bool (* dm_for_each_cb)(const pcm_path_t* path, amxc_var_t* value, pcm_dataset_t dataset, void* userdata);

/**
 * @brief get amxd object
 *
 * @param curr the root object
 * @param path the path up to a parameter or object
 * @param offset offset in the path
 * @param with_param the path contains a parameter
 * @return the amxd object
 */
amxd_object_t* pcm_dm_get_object(amxd_object_t* curr, const pcm_path_t* path, int offset, bool with_param);

/**
 * @brief Adds an instance add action to a transaction. The absolute path of the instance must be provided in the path
 * argument.
 *
 *
 * @param curr the root object
 * @param path the path up to a parameter or object
 * @param offset offset in the path
 * @param trans pointer to a transaction
 * @return `true` if the instance is added successfully to the transaction, otherwise it returns `false`
 */
bool pcm_dm_add_inst(amxd_object_t* curr, const pcm_path_t* path, int offset, amxd_trans_t* trans);

/**
 * @brief Set a parameter to a value
 *
 * @details
 * If the path up to the parameter does not yet exist, then
 * new objects are created only _IF_ they are instance objects.
 *
 * @param object the root object
 * @param path the path up to a parameter
 * @param offset offset in the path
 * @param value the value to set
 * @param dataset the dataset to manipulate
 * @return true upon success, false otherwise
 */
bool pcm_dm_set(amxd_object_t* object, const pcm_path_t* path, int offset, amxc_var_t* value, pcm_dataset_t dataset);

/**
 * @brief Remove an object
 *
 * @de
 * Only removal of instance objects is allowed.
 * If the path points to an instance, that instance is deleted.
 * If it points to a template, all its instances are deleted.
 *
 * @param object the root object
 * @param path the path up to an object
 * @param offset offset in the path
 * @param dataset the dataset to manipulate
 * @return true upon success, false otherwise
 */
bool pcm_dm_object_remove(amxd_object_t* object, const pcm_path_t* path, int offset, pcm_dataset_t dataset);

/**
 * @brief For each parameter in the data model
 *
 * @param object the root object
 * @param usersetting loop over usersettings
 * @param changed loop over changed settings
 * @param cb the callback function
 * @param userdata user data
 * @return true upon success, false otherwise
 */
bool pcm_dm_for_each(amxd_object_t* object, bool usersetting, bool changed, dm_for_each_cb cb, void* userdata);

#ifdef __cplusplus
}
#endif

#endif // __MOD_PCM_DM_H__
