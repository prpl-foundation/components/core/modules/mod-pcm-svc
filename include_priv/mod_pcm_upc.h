/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_PCM_UPC_H__)
#define __MOD_PCM_UPC_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include "mod_pcm_def.h"
#include "mod_pcm_path.h"

/**
 * @brief Callback prototype to be used with pcm_upc_for_each()
 *
 * @param path the path up to a leaf
 * @param value the leaf's value
 * @param dataset the dataset of the leaf
 * @param userdata user data
 * @return true upon success, false otherwise
 */
typedef bool (* upc_forEach_cb)(const pcm_path_t* path, amxc_var_t* value, pcm_dataset_t dataset, void* userdata);

/**
 * @brief Create UPC data
 *
 * @details
 * The UPC data will be allocated on the heap.
 * Call pcm_upc_destroy() after usage.
 *
 * @param usersetting the UPC data are user settings
 * @param userflags stricted userflags.
 * @return UPC data upon success, NULL otherwise
 */
pcm_upc_t* pcm_upc_create(bool usersetting, amxc_string_t* userflag);

/**
 * @brief Destroy UPC data
 *
 * @details
 * The UPC data will be freed.
 *
 * @param data the UPC data
 */
void pcm_upc_destroy(pcm_upc_t* data);

/**
 * @brief Initialize UPC data
 *
 * @param data the UPC data
 * @param usersetting the UPC data are user settings
 * @param userflags stricted userflags.
 * @return true upon success, false otherwise
 */
bool pcm_upc_initialize(pcm_upc_t* data, bool usersetting, amxc_string_t* userflags);

/**
 * @brief Clean up UPC data
 *
 * @param data the UPC data
 */
void pcm_upc_cleanup(pcm_upc_t* data);

/**
 * @brief Verify whether UPC data is valid
 *
 * @param data the UPC data
 * @return true upon success, false otherwise
 */
bool pcm_upc_is_valid(const pcm_upc_t* data);

/**
 * @brief Set a leaf to a value
 *
 * @details
 * Note: the type of 'value' can only be "variant_type_unknown" in case
 * the 'dataset' is "pcm_dataset_del".
 *
 * @param data the UPC data
 * @param path the path up to a leaf
 * @param value the value to set
 * @param dataset the dataset to manipulate
 * @return true upon success, false otherwise
 */
bool pcm_upc_set(amxc_var_t* data, const pcm_path_t* path, amxc_var_t* value, pcm_dataset_t dataset);

/**
 * @brief For each leaf in the UPC data
 *
 * @param data the UPC data
 * @param cb the callback function
 * @param userdata user data
 * @return true upon success, false otherwise
 */
bool pcm_upc_for_each(const pcm_upc_t* data, upc_forEach_cb cb, void* userdata);

amxc_var_t* pcm_add_element(amxc_var_t* curr, uint16_t type_id, const char* data);
amxc_var_t* pcm_get_element(amxc_var_t* curr, const char* data);

#ifdef __cplusplus
}
#endif

#endif // __MOD_PCM_UPC_H__
