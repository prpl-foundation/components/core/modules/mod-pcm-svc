/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MOD_PCM_INST_H__)
#define __MOD_PCM_INST_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>

#include "mod_pcm_path.h"

/**
 * @brief Add a new instance to the instances list
 *
 * @param instances a variant list representing the instances
 * @param path the path of the instance
 * @param name the name of the instance
 * @param type the type of the instance [upc, usersetting]
 * @param primkey the primary key of the instance
 * @param primkeyvalue the primary key value of the instance
 */
void pcm_inst_add(amxc_var_t* instances,
                  const char* path,
                  const char* name,
                  const char* type,
                  const char* primkey,
                  const char* primkeyvalue);

/**
 * @brief Remove a matched instance from the instances list
 *
 * @details
 * A first match is done on the path and primary key.
 * If no primary key is avaialble the name will be used.
 *
 * @param instances a variant list representing the instances
 * @param path the path of the instance
 * @param name the name of the instance
 */
void pcm_inst_remove(const amxc_var_t* instances,
                     const char* path,
                     const char* name);

/**
 * @brief Find and move matching instances to the destination list
 *
 * @details
 * We fist use findpath to see or we have matching instances in the source list.
 * If a match is found, it will be added to the destination list.
 *
 * @param findpath the path of the instance
 * @param src_instances the source list
 * @param dst_instances the destination list
 */
void pcm_inst_findpath(const pcm_path_t* findpath,
                       const amxc_var_t* src_instances,
                       amxc_var_t* dst_instances);

/**
 * @brief Check if the instance list contains the given instance. If so, it returns true,
 * otherwise returns false.
 *
 * @param path the path of the instance
 * @param name the name of the instamce
 * @param src_instances the source list
 */
bool pcm_svc_is_run_time(const char* path,
                         const char* name,
                         amxc_var_t* src_instances);
/**
 * @brief Write the instance list to a file
 *
 * @param file the full file path to write to
 * @param instances the instance list
 * @return the size of the file that was written
 */
int pcm_inst_write(const char* file, const amxc_var_t* instances);

/**
 * @brief Read the instance list from the file into a variant list
 *
 * @param file the full file path to read from
 * @param instances the instance list
 */
void pcm_inst_read(const char* file, amxc_var_t* const instances);

#ifdef __cplusplus
}
#endif

#endif // __MOD_PCM_INST_H__
