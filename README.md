# Module PCM_SVC

## Using mod_pcm_svc

In the main odl you should include `mod_pcm_svc.odl` preferably as last line

```odl
#include "mod_pcm_svc.odl";
```

## Configuration

### config options

mod_pcm_svc config options should be provided in `%config` section of main odl file.

The following config options are supported:
 
- `pcm_svc_config`:  a table that can contain
   - `Objects`: Comma separated list of objects which need to be registered with pcm_manager [Mandatory]
   - `Import`: Function to be used to import stored configuration from `pcm` manager.  If not provided `PcmImport` is registered [Optional]
   - `Export`: Function to be used to export configuration to `pcm` manager. If not provided `PcmExport` is registered [Optional]
   - `BackupFiles`: Comma separated list of alternative backup files. They are in correspondence with the available `Objects`. [Optional]

#### Example

```odl
%config {
    pcm_svc_config = {
        "Objects" = "Firewall,NAT", 
        "Import" = "DummyImport",
        "Export" = "DummyExport",
        "BackupFiles" = "alt_backup_for_Firewall,alt_backup_for_NAT"
    };
    
    ...
    
    #include "mod_pcm_svc.odl";
}
```

### Note

`mod_pcm` set global ubus config parameter `register-on-start-event` to true so functions `PcmExport` and `PcmImport` could be accessible via `amxb_call`

#### register-on-start-event
Register all known data model objects to ubus daemon when app:start event is received. 
This event is send right before the event loop is started. When this option is not set the 
data model objects are registered to the ubus daemon just before the entry-points are called.